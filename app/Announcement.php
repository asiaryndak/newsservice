<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    //
    protected $primaryKey = 'announcementId';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'announcementTitle','announcementContent','announcementType','announceAuthorId','contactType','phone','announceAuthorId','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'rememberToken',
    ];


    public function articlesAttachments(){
       return $this->hasMany('App\ArticleAttachment');
    }

    public function user(){
       return $this->belongsTo('App\User');
    }

    public static function createAnnounce($announcementTitle,$announcementType,$contactType,$phone,$announcementAuthorId,$announcementContent){
        $newAnnounce= new Announcement(['announcementTitle'=>$announcementTitle,'announcementType'=>$announcementType,'contactType'=>$contactType,'phone'=>$phone,'announceAuthorId'=>$announcementAuthorId,'announcementContent'=>$announcementContent]);
        $newAnnounce->save();
        return $newAnnounce;
    }

    public static function refreshStartDate($announcementId){
        $announce=Announcement::find($announcementId);
        $startDate = Carbon::now()->toDateTimeString();
        $announce->update(['updated_at'=>$startDate]);
        $announce->save();
        return $announcementId;
    }
}

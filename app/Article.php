<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $primaryKey = 'articleId';
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'articleTitle','articleContent','articleType','articleAuthorId','articleViewCount','articleStatus','articleCategory',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'rememberToken',
    ];

    public static function createArticle($articleTitle,$articleContent,$articleViewCount,$articleAuthorId,$articleCategory,$articleStatus){
        $newArticle = new Article(['articleTitle'=>$articleTitle,'articleContent'=>$articleContent,'articleViewCount'=>$articleViewCount,'articleAuthorId'=>$articleAuthorId,'articleCategory'=>$articleCategory,'articleStatus'=>$articleStatus]);
        $newArticle->save();
        return $newArticle;
    }

    public static function updateArticle($articleId,$articleTitle,$articleContent,$articleCategory){
        $article=Article::find($articleId);
        $article->update(['articleTitle'=>$articleTitle,'articleContent'=>$articleContent,'articleCategory'=>$articleCategory]);
        $article->save();
        return $articleId;
    }
    /**
     * Publiczna funkcja bedaca powiazaniem miedzy tabela artykuly, a tabela uzytkownikow(Users)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**Publiczna funkcja bedaca powiazaniem miedzy tabela artykuly, a tabela zalaczniki do artykulow(ArticleAttachments)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articlesAttachments(){
        return $this->hasMany('App\ArticleAttachment');
    }

    /**Publiczna funkcja bedaca powiazaniem miedzy tabela artykuly, a tabela komentarzy(Comment)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public static function showMyArticles($id){
        $articles = DB::table('articles')->where('articleAuthorId','=',$id)->get();

    }
    public static function showAllArticles(){

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleAttachment extends Model
{
    protected $primaryKey = 'attachmentId';
    public $incrementing = true;
    public $timestamps =false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attachmentLink','attachmentType','articleId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'rememberToken',
    ];

    public function articles(){
        $this->belongsTo('App\Article');
    }

    public function announcements(){
        $this->belongsTo('App\Announcement');
    }

    public static function createArticlesAttachment($attachmentLink,$attachmentType,$articleId){
        $articlesAttachment = new ArticleAttachment(['attachmentLink'=>$attachmentLink,'attachmentType'=>$attachmentType,'articleId'=>$articleId]);
        $articlesAttachment->save();
        return $articlesAttachment;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $primaryKey = 'commentId';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commentText','articleId','commentAuthorId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'rememberToken',
    ];

    public static function createComment($commentText,$articleId,$commentAuthorId){
        $newComment = new Comment(['commentText'=>$commentText,'articleId'=>$articleId,'commentAuthorId'=>$commentAuthorId]);
        $newComment->save();
    }

    /**Publiczna funkcja bedaca powiazaniem miedzy tabela komentarze, a tabela artykulow(Article)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function articles(){
        return $this->belongsTo('App\Article');
    }
}

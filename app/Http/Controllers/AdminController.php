<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Article;
use App\ArticleAttachment;
use App\Comment;
use App\Mail\AdminInfo;
use App\Mail\ConfirmArticle;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;

class AdminController extends Controller
{
    public function showLastRegisteredUsers(){
        $users = User::orderBy('created_at','desc')->paginate(3);
        return view('adminviews.showLastRegisteredUsers')->with('users',$users);
    }

    public function makeUserAccountActive($userId){
        $user = User::find($userId);
        if($user<>null) {
            $user->update(['activateStatus' => 1]);
            $user->save();
            if ($user) {
                $title = 'Twoje konto zostało aktywowane';
                $text = 'Administrator serwisu bestNews.pl aktywował Twoje konto. Możesz się już zalogować.';
                $userMail = $user->email;
                $content = [
                    'subject' => $title,
                    'user' => $user,
                    'text' => $text
                ];
                Mail::to($user)->send(new ConfirmArticle($content));
                return back()->with('message', "Konto użytkownika " . $user->name . " " . $user->lastName . " zostało aktywowane poprawnie");
            } else
                return back()->with('message', "Aktywacja konta użytkownika '" . $user->name . "' '" . $user->lastName . "' nie powiodła się");
        }
        else{
            return abort(404);
        }
    }

    public function makeUserAccountDeactivate($userId){
        $user = User::find($userId);
        if($user<>null) {
            $email = $user->email;
            $user->update(['activateStatus' => 0]);

            $user->save();
            $title = 'Twoje konto zostało dezaktywowane';
            $text = 'Administrator serwisu bestNews.pl dezaktywował Twoje konto.
        Prawdopodobnie jest to spowodowane naruszeniem regulaminu. 
        Jeśli uważasz, że to pomyłka, skontaktuj się z nami na adres tego maila.';
            $userMail = $user->email;
            $content = [
                'subject' => $title,
                'user' => $user,
                'text' => $text
            ];
            Mail::to($user)->send(new ConfirmArticle($content));
            return back()->with('message', "Konto użytkownika " . $user->name . " " . $user->lastName . " zostało dezaktywowane");
        }else{
            return abort(404);
        }

    }

    public function showUserActivity($userId){
        $user = User::find($userId);
        if($user<>null) {
            $articles = Article::where('articleAuthorId', $userId)->get();
            $announcements = Announcement::where('announceAuthorId', $userId)->get();
            $comments = Comment::where('commentAuthorId', $userId)->get();

            return view('adminviews.usersManaging.showUserActivity')->with(['user' => $user, 'articles' => $articles, 'announcements' => $announcements, 'comments' => $comments]);
        }
        else{
            return abort(404);
        }
    }

    public function rejectArticle($articleId){
        $article=Article::find($articleId);
        if($article<>null) {
            $article->update(['articleStatus' => 'rejected']);
            $article->save();
            if ($article) {
                $title = "Twój artykuł '$article->articleTitle' został odrzucony.";
                $text = 'Administrator serwisu bestNews.pl odrzucił Twój artykuł i tym samym nie będzie on widoczny dla innych użytkowników serwisu.';
                $user = User::find($article->articleAuthorId);
                $userMail = $user->email;
                $content = [
                    'subject' => $title,
                    'user' => $user,
                    'text' => $text
                ];
                Mail::to($user)->send(new ConfirmArticle($content));
                return back()->with('message', "Artykuł " . $article->articleTitle . " został odrzucony");
            } else
                return back()->with('message', 'Operacja odrzucania nie powiodła się.');
        }else{
            return abort(404);
        }
    }

    public function archiveArticle($articleId){
        $article=Article::find($articleId);
        if($article<>null) {
            $article->update(['articleStatus' => 'archive']);
            $article->save();
            if ($article) {
                $title = "Twój artykuł '$article->articleTitle' został zarchiwizowany.";
                $text = 'Administrator serwisu bestNews.pl zarchiwizował Twój artykuł.';
                $user = User::find($article->articleAuthorId);
                $userMail = $user->email;
                $content = [
                    'subject' => $title,
                    'user' => $user,
                    'text' => $text
                ];
                Mail::to($user)->send(new ConfirmArticle($content));
                return back()->with('message', "Artykuł " . $article->articleTitle . " został zarchiwizowany");
            } else
                return back()->with('message', 'Archiwizowanie nie powiodło się.');
        }else{
            return abort(404);
        }
    }

    public function unarchiveArticle($articleId){
        $article=Article::find($articleId);
        if($article<>null){
        $article->update(['articleStatus'=>'waiting']);
        $article->save();
        if($article){
            $title="Twój artykuł '$article->articleTitle' został przywrócony z archiwum.";
            $text='Administrator serwisu bestNews.pl przywrócił Twój artykuł z archiwum.';
            $user=User::find($article->articleAuthorId);
            $userMail = $user->email;
            $content= [
                'subject'=>$title,
                'user'=>$user,
                'text'=>$text
            ];
            Mail::to($user)->send(new ConfirmArticle($content));
            return back()->with('message',"Artykuł ".$article->articleTitle." został przywrócony z archiwum");}
        else
            return back()->with('message','Przywrócenie nie powiodło się.');}
            else{
            return abort(404);
            }
    }

    public function confirmArticle($articleId){
        $article=Article::find($articleId);
        $article->update(['articleStatus'=>'confirmed']);
        $article->save();

        $title="Twój artykuł '$article->articleTitle' został zaakceptowany.";
        $text='Administrator serwisu bestNews.pl zaakceptował Twój artykuł.';
        $user=User::find($article->articleAuthorId);
        $userMail = $user->email;

        $content= [
            'subject'=>$title,
            'user'=>$user,
            'text'=>$text
        ];
        Mail::to($user)->send(new ConfirmArticle($content));



//        $this->sendMail($title,$content,$userMail);
            return back()->with('message',"Artykuł ".$article->articleTitle." został zatwierdzony");
    }

    public function deleteArticleView($articleId){
        $article=Article::find($articleId);
        if($article<>null){
        \Session::put('backUrl',\URL::previous());

        return view('adminviews.usersManaging.deleteArticleView')->with('article',$article);}
        else{
            return abort(404);
        }
    }

    public function deleteArticle($id){
        $article=Article::find($id);
        $articleAttachments=ArticleAttachment::where('articleId',$id);
        foreach ($articleAttachments as $articleAttachment){
            $src=$articleAttachment->attachmentLink;
            ArticleAttachment::find($articleAttachment->attachmentId)->delete();
            \Storage::delete(asset($src));
        }
        Article::find($id)->delete();
        $title="Twój artykuł '$article->articleTitle' został usunięty.";
        $text='Administrator serwisu bestNews.pl usunął Twój artykuł.';
        $user=User::find($article->articleAuthorId);
        $userMail = $user->email;
//        $this->sendMail($title,$content,$userMail);
        $content= [
            'subject'=>$title,
            'user'=>$user,
            'text'=>$text
        ];
        Mail::to($user)->send(new ConfirmArticle($content));
        $url = \Session::get('backUrl');
        return redirect($url)->with('message', 'Artykuł został usunięty poprawnie');
    }

    public function deleteArticleCancel(){
        $url = \Session::get('backUrl');
        return redirect($url);
    }

    public function deleteAnnouncement($announcementId){
        $announcement = Announcement::find($announcementId);
        if($announcement<>null){
        $announcementTitle = $announcement->announcementTitle;
        if((\Auth::user()->status=='admin')||\Auth::user()->id==$announcement->announceAuthorId) {
            $announcement->delete();

            $title = "Twoje ogłoszenie '$announcementTitle' zostało usunięte.'";
            $text = 'Twoje ogłoszenie zostało usunięte z serwisu bestNews.pl';
            $user = User::find($announcement->announceAuthorId);
            $userMail = $user->email;
            $content = [
                'subject' => $title,
                'user' => $user,
                'text' => $text
            ];
            Mail::to($user)->send(new ConfirmArticle($content));

            return back()->with('message', "Ogłoszenie " . $announcementTitle . " zostało usunięte");
        }
        else{
            return abort(404);
        }
        }
        else return view('errors.404');
    }

    public function showUserDetails($userId){
        $user = User::find($userId);
        return view('adminviews.usersManaging.userDetails')->with('user',$user);
    }

    public function editUserView($userId){
        \Session::put('backUrl',\URL::previous());
        $user=User::find($userId);
        if($user<>null) {
            return view('adminviews.usersManaging.editUserForm')->with('user', $user);
        }
        else{
            return abort(404);
        }
    }

    public function editUser(Request $request){
        $id=$request->userId;
        $user=User::find($id);
        $this->validate($request, [
            'email' => 'required|unique:users,email,'.$user->id,
        ], [], []);


        $name=$request->name;
        $lastName=$request->lastName;
        $email=$request->email;
        $status=$request->status;


        $user->update(['name'=>$name,'lastName'=>$lastName,'email'=>$email,'status'=>$status]);
        $user->save();
        $title="Twoje dane w naszym profilu zostały zedytowane.";
        $text='Dane w serwisie bestNews.pl zostały zaktualizowane poprawnie.';
        $userMail = $user->email;
        $content= [
            'subject'=>$title,
            'user'=>$user,
            'text'=>$text
        ];
        Mail::to($user)->send(new ConfirmArticle($content));
        $url = \Session::get('backUrl');
        return redirect($url)->with('message',"Dane o użytkowniku ".$name." ".$lastName." zostały zedytowane poprawnie.");
    }

    public function deleteUserView($id){
        $user=User::find($id);
        \Session::put('backUrl',\URL::previous());

        return view('adminviews.usersManaging.deleteUserView')->with('user',$user);
    }

    public function deleteUserConfirm($id){
        $user=User::find($id);
        $name=$user->name;
        $lastName=$user->lastName;
        $user->delete();
        $url = \Session::get('backUrl');
        return redirect($url)->with('message',"Użytkownik ".$name." ".$lastName." został usunięty.");
    }

    public function deleteUserCancel(){
        $url = \Session::get('backUrl');
        return redirect($url);
    }

    public function manageArticlesView(){
        $lastArticles = Article::paginate(5);
        if($lastArticles<>null) {
            return view('adminviews.articlesManaging.manageArticles')->with('lastArticles', $lastArticles);
        }
        else{
            return abort(404);
        }
    }

    public function serviceUsers(){
        if(\Session::get('flashed')==='active'){
            $users=User::where('activateStatus','1')->paginate(5);
        }
        elseif(\Session::get('flashed')==='unactive'){
            $users=User::where('activateStatus','0')->paginate(5);
        }
        else {
            $users = User::paginate(5);
        }
        return view('adminviews.usersManaging.serviceUsers')->with('users',$users);
    }

    public function showUnactiveUsersAccounts(){
        \Session::flash('flashed','unactive');
        return redirect()->back();
    }

    public function showActiveUsersAccounts(){
        \Session::flash('flashed','active');
        return redirect()->back();
    }

    public function showAllUsers(){
        return redirect()->back();
    }
}

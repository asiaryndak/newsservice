<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Article;
use App\ArticleAttachment;
use App\Comment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainPageController extends Controller
{
    //
    public function log()
    {
        // routing na stronę logowania
        return view('redactorviews.redactorHome');
    }

    public function home()
    {
        if (!(Auth::user())) {
            $today=Carbon::now();
            $weekAgo=$today->subDays(7)->toDateString();
            $monthAgo=$today->subDays(30)->toDateString();
            $allUsersCount=User::count();
            $usersRegistratedLastWeek=User::whereDate('created_at','>',$weekAgo)->count();
            $articlesConfirmed = Article::where('articleStatus','confirmed')->paginate(6,['*'], 'articlesConfirmed');
            $articlesCuriosityConfirmed = Article::where('articleStatus','confirmed')->where('articleCategory','curiosity')->paginate(2,['*'], 'curiosity');
            $articlesLastWeek = Article::where('created_at','>',$weekAgo)->paginate(2,['*'], 'articlesLastWeek');
            $lastAddComments=Comment::orderBy('created_at','desc')->paginate(2,['*'], 'lastAddComments');
            $lastActiveAnnouncements=Announcement::where('updated_at','>',$weekAgo)->orderBy('updated_at','desc')->paginate(2,['*'],'announcements');
            return view('userviews.unloggeduserviews.userMainPage')->with(['allUsersCount'=>$allUsersCount,'usersRegistratedLastWeek'=>$usersRegistratedLastWeek,
                'articlesConfirmed'=>$articlesConfirmed,'articlesLastWeek'=>$articlesLastWeek,'lastAddComments'=>$lastAddComments,
                'lastActiveAnnouncements'=>$lastActiveAnnouncements,'articlesCuriosityConfirmed'=>$articlesCuriosityConfirmed]);
        } else {
        if (Auth::user()->status === 'admin') {

            $today=Carbon::now();
            $weekAgo=$today->subDays(7)->toDateString();
            $monthAgo=$today->subDays(30)->toDateString();

            $allUsersCount=User::count();
            $usersCount=User::where('status','user')->count();
            $usersRegistratedLastWeek=User::whereDate('created_at','>',$weekAgo)->paginate(2,['*'], 'users');

            $redactorsCount=User::where('status','writer')->count();
            $idAuthorOfMostCountConfirmedArticlesLastMonth = Article::whereDate('created_at','>',$monthAgo)->where('articleStatus','confirmed')->groupBy('articleAuthorId')->select('articleAuthorId',\DB::raw('count(*) as total'))->orderBy('total','desc')->limit(1)->get();
            foreach($idAuthorOfMostCountConfirmedArticlesLastMonth as $authorId){
                $mostArticlesOneRedactorLastMonth = Article::whereDate('created_at','>',$monthAgo)->where('articleAuthorId',$authorId->articleAuthorId)->paginate(2,['*'], 'articles');
            }


            $adminsCount=User::where('status','admin')->count();

            $allArticlesCount =Article::count();
            $articlesConfirmedCount=Article::where('articleStatus','confirmed')->count();
            $articlesConfirmed = Article::where('articleStatus','confirmed')->paginate(2,['*'], 'articlesConfirmed');
            $articlesWaitingCount=Article::where('articleStatus','waiting')->count();
            $articlesWaiting=Article::where('articleStatus','waiting')->paginate(2,['*'], 'articlesWaiting');

            $articlesLastWeekCount=Article::where('created_at','>',$weekAgo)->count();
            $articlesLastWeek = Article::where('created_at','>',$weekAgo)->paginate(2,['*'], 'articlesLastWeek');

            $lastAddComments=Comment::orderBy('created_at','desc')->paginate(2,['*'], 'lastAddComments');

return view('adminviews.adminHome')->with(['allUsersCount'=>$allUsersCount,'usersCount'=>$usersCount,
    'redactorsCount'=>$redactorsCount,'adminsCount'=>$adminsCount,'allArticlesCount'=>$allArticlesCount,
    'articlesConfirmedCount'=>$articlesConfirmedCount,'articlesConfirmed'=>$articlesConfirmed,
    'articlesWaitingCount'=>$articlesWaitingCount,'articlesWaiting'=>$articlesWaiting,'articlesLastWeekCount'=>$articlesLastWeekCount,
    'articlesLastWeek'=>$articlesLastWeek,'lastAddComments'=>$lastAddComments,'usersRegistratedLastWeek'=>$usersRegistratedLastWeek,
    'idAuthorOfMostCountConfirmedArticlesLastMonth'=>$idAuthorOfMostCountConfirmedArticlesLastMonth,
    'mostArticlesOneRedactorLastMonth'=>$mostArticlesOneRedactorLastMonth]);


        } elseif (Auth::user()->status === 'user') {
            $today=Carbon::now();
            $weekAgo=$today->subDays(7)->toDateString();
            $monthAgo=$today->subDays(30)->toDateString();
            $allUsersCount=User::count();
            $usersRegistratedLastWeek=User::whereDate('created_at','>',$weekAgo)->count();
            $articlesConfirmed = Article::where('articleStatus','confirmed')->where('articleCategory','news')->paginate(6,['*'], 'articlesConfirmed');
            $articlesCuriosityConfirmed = Article::where('articleStatus','confirmed')->where('articleCategory','curiosity')->paginate(2,['*'], 'curiosity');
            $articlesLastWeek = Article::where('created_at','>',$weekAgo)->where('articleCategory','news')->paginate(2,['*'], 'articlesLastWeek');
            $lastAddComments=Comment::orderBy('created_at','desc')->paginate(2,['*'], 'lastAddComments');
            $lastActiveAnnouncements=Announcement::where('updated_at','>',$weekAgo)->orderBy('updated_at','desc')->paginate(2,['*'],'announcements');
            return view('userviews.unloggeduserviews.userMainPage')->with(['allUsersCount'=>$allUsersCount,'usersRegistratedLastWeek'=>$usersRegistratedLastWeek,
                'articlesConfirmed'=>$articlesConfirmed,'articlesLastWeek'=>$articlesLastWeek,'lastAddComments'=>$lastAddComments,
                'lastActiveAnnouncements'=>$lastActiveAnnouncements,'articlesCuriosityConfirmed'=>$articlesCuriosityConfirmed]);
        }elseif(Auth::user()->status ==='writer'){
            $today=Carbon::now();
            $weekAgo=$today->subDays(7)->toDateString();
/*            $yourMostOftenCommentedArticles= DB::table('articles')
                ->join('comments', 'articles.articleId','=','comments.articleId')
                ->select('articles.*','comments.commentText','comments.commentAuthorId','comments.created_at')
                ->groupBy('articles.articleId')
//                ->orderByRaw('count (*) as total')
                ->get();*/
//            $yourMostOftenCommentedArticles=Article::where(Comment::count()->where('articleId',(Article::where('articleAuthorId',Auth::user()->id)))->groupBy('articleId')->orderBy(count(),'desc'))->get();
            $yoursAcceptedArticles = Article::where('articleAuthorId',Auth::user()->id)->where('articleStatus','confirmed')->orderBy('created_at','desc')->paginate(2,['*'],'acceptedArticles');
            $yoursDeclineArticles = Article::where('articleAuthorId',Auth::user()->id)->where('articleStatus','rejected')->orderBy('created_at','desc')->paginate(2,['*'],'rejectedArticles');
            $yoursAnnouncements = Announcement::where('announceAuthorId',Auth::user()->id)->whereDate('updated_at','>',$weekAgo)->orderBy('created_at','desc')->paginate(2,['*'],'announcementsActive');
            $yoursAnnouncementsOff = Announcement::where('announceAuthorId',Auth::user()->id)->whereDate('updated_at','<',$weekAgo)->orderBy('created_at','desc')->paginate(2,['*'],'announcementsOff');



            return view('redactorviews.redactorHome')->with(['yoursAcceptedArticles'=>$yoursAcceptedArticles,'yoursDeclineArticles'=>$yoursDeclineArticles,'yoursAnnouncements'=>$yoursAnnouncements,'yoursAnnouncementsOff'=>$yoursAnnouncementsOff]);
        }
    }
}
}

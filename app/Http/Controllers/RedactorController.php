<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Article;
use App\ArticleAttachment;
use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;
//use Illuminate\Support\Facades\Storage;


//use App\Http\Requests\CheckOrderRequest;

class RedactorController extends Controller
{
    /** Metoda do dodawania artykułu wraz ze zdjęciem
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addArticle(Request $request){

            $title = $request->articleTitle;
            $content = $request->input('wysihtml5-textarea');
            $type = $request->typeOfArticle;
//            return $type;
            $status = 'waiting';
            $author = \Auth::user()->id;

        if(\Auth::user()->status=='admin'){
            return abort(404);
        }
        else {
            $article = Article::createArticle($title, $content, null, $author, $type, $status);
            $lastAddedArticleId = $article->articleId;

                    $this->validate($request, [
                        'input_img' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ],[],
                        []);
            if ($request->hasFile('input_img')) {
                $image = $request->file('input_img');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('../storage/app/public/images');
                $destinationPath = public_path('images');
                $image->move($destinationPath, $name);
                $attachmentLink = '/images/' . $name;
                $articleAttachment = ArticleAttachment::createArticlesAttachment($attachmentLink, 'image', $lastAddedArticleId);
            }
            \Session::flash('message', "Artykuł '" . $title . "' został dodany");
            return redirect()->route('showMyArticles');
        }

    }

    public function addArticleFormView(){
        if(\Auth::user()->status=='admin'){
            return abort(404);
        }
        else {
            return view('redactorviews.addArticleForm');
        }
    }

    public function getArticle(){
        $articles = DB::table('articles')->get();
        foreach ($articles as $article) {
            echo $article->articleContent;
        }
    }
    public function showMyArticles(){
        $articles = Article::where('articleAuthorId',\Auth::user()->id)->paginate(3);
        return view('redactorviews.showMyArticles')->with('articles',$articles);
    }


    public function showLastAddedArticles(){
        $lastArticles = Article::orderBy('updated_at','desc')->paginate(3);
        return view('redactorviews.showLastAddedArticles')->with('lastArticles',$lastArticles);
    }


    /** Funkcja do wyswietlania szczegolow konkretnych artykulow
     * @param $articleId
     */
    public function articleDetails($articleId){
        $article = Article::find($articleId);
        if($article<>null) {
            $articleAttachments = ArticleAttachment::where('articleId', $articleId)->get();
            $comments = Comment::where('articleId', $articleId)->orderBy('created_at', 'desc')->get();
            return view('redactorviews.articleDetails')->with(['article' => $article, 'articleAttachments' => $articleAttachments, 'comments' => $comments]);
        }
        else{
            return abort(404);
        }
    }


    /**Funkcja do edytowania artykulu o konkretnym id w bazie danych
     * @param $id
     */
    public function editArticle($id){
        $article = Article::find($id);
        if($article<>null) {
            if (($article->articleAuthorId == \Auth::user()->id) || (\Auth::user()->status == 'admin')) {
                $articleAttachments = ArticleAttachment::where('articleId', $id)->get();
                return view('redactorviews.editArticleForm')->with(['article' => $article, 'articleAttachments' => $articleAttachments]);
            } else return abort(404);
        }
        else{
            return abort(404);
        }

    }

    public function updateDataInArticle(Request $request){
        $articleId=$request->articleId;
        $articleTitle=$request->articleTitle;
        $articleContent=$request->articleContent;
//        $articleContent=$request->articleContent;
        $articleCategory=$request->typeOfArticle;
        $userId=$request->userId;
        $user= User::find($userId);

        $article = Article::find($articleId);
        if($article<>null) {
            if (($article->articleAuthorId == $userId) || ($user->status == 'admin')) {

                Article::updateArticle($articleId, $articleTitle, $articleContent, $articleCategory);
                \Session::flash('message', "Edycja artykułu '" . $articleTitle . "' zakończyła się pomyślnie");
                return redirect("articleDetails/$articleId");
            }else return abort(404);
        }else{
            return abort(404);
        }
    }

    /**Funkcja do usuwania artykulu o konkretnym id w bazie danych
     * @param $id
     */
    public function deleteArticle($id){
        $article=Article::find($id);
        if($article<>null){
            if(\Auth::user()->id===$article->articleAuthorId){
                $articleAttachments=ArticleAttachment::where('articleId',$id);
                foreach ($articleAttachments as $articleAttachment){
                    $src=$articleAttachment->attachmentLink;
                    Storage::delete(asset($src));
//            Storage::delete($src);
                }
//        $articleDelete=Article::delete($article);
                Article::find($id)->delete();
                \Session::flash('message',"Artykuł '".$article->articleTitle."' został usunięty");

                return redirect('/showMyArticles');
            }
        }else{
            return abort(404);
        }

    }

    public function addAnnounceForm(){
        if(\Auth::user()->status==='admin'){
            return abort(404);
        }
        return view('redactorviews.addAnnounceForm');
    }

    public function addAnnounce(Request $request){

        $title = $request->announcementTitle;
        $aType = $request->announcementType;
        $contactType = $request->contactType;
        $phone = $request->phone;
        $content = $request->announcementContent;
        $author = $request->userId;
        $announcement = Announcement::createAnnounce($title,$aType,$contactType,$phone,$author,$content);
        \Session::flash('message',"Ogłoszenie ''".$title."'' zostało dodane");
        return redirect('showLastAddAnnouncements');
    }

    public function showLastAddedAnnouncements(){
        $lastAnnouncements = Announcement::orderBy('updated_at','desc')->paginate(3);
        return view('redactorviews.showLastAddedAnnouncements')->with('lastAnnouncements',$lastAnnouncements);
    }

    public function announceDetails($announcementId){
        $announcement = Announcement::find($announcementId);
        if($announcement<>null) {
            return view('redactorviews.announcementDetails')->with('announcement', $announcement);
        }
        else{
            return abort(404);
        }
    }

    public function refreshAnnouncement($id){
        $refreshAnnounce = Announcement::refreshStartDate($id);
        $title = Announcement::find($id)->announcementTitle;
        \Session::flash('message',"Ogłoszenie ''".$title."'' zostało odnowione i będzie aktywne przez kolejne 7 dni");
//        $this->showLastAddedAnnouncements();
        return redirect('showLastAddAnnouncements');
    }

    public function showUserAnnouncements($id){
        $userAnnouncements = Announcement::where('announceAuthorId',$id)->paginate(3);
        return view('redactorviews.showUserAnnouncements')->with('userAnnouncements',$userAnnouncements);
    }

    public function showAnnouncementsFromCategory($category){
//        $announcements = DB::table('announcements')->where('announcementType',$category)->paginate(3);
        $announcements = Announcement::where('announcementType',$category)->paginate(3);
        return view('redactorviews.showAnnouncementsFromCategory')->with('announcements',$announcements);
    }


    public function addCommentToArticle(Request $request){
        $text=$request->commentText;
        $authorId=$request->commentAuthorId;
        $articleId = $request->articleId;

        $comment=Comment::createComment($text,$articleId,$authorId);
        \Session::flash('message',"Komentarz do artykułu został dodany");
        if(\Auth::user()->status=='admin'||(\Auth::user()->status=='writer')){
        return redirect("articleDetails/$articleId");}
        else
            return redirect("userArticleDetails/$articleId");
    }

    public function deleteMyCommentToArticle($id){
        $comment = Comment::find($id)->delete();
//        if($comment->commentAuthorId===\Auth::user()->id||\Auth::user()->status==='admin') {
            return redirect()->back()->with('message', 'Komentarz został usunięty');
/*        }
        else{
            return abort(404);
        }*/
    }



}

<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Article;
use App\ArticleAttachment;
use App\Comment;
use App\Mail\ContactMail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class UserController extends Controller
{
    public function showMyProfile($id)
    {
        $user = User::find($id);
        if($user<>null) {
            return view('redactorviews.myProfile')->with('user', $user);
        }
        else{
            return abort(404);
        }
    }

    public function editMyProfileView($id)
    {
        $user = User::find($id);
        return view('userviews.editMyProfileForm')->with('user', $user);
    }

    public function updateProfile(Request $request){
        $id=$request->userId;
        $name=$request->name;
        $lastName=$request->lastName;
        $email=$request->email;
        $update=User::updateProfileData($id,$name,$lastName,$email);
        \Session::flash('message','Edycja danych profilowych zakończona pomyślnie.');
        return redirect("myProfile/$id");
    }

    public function changePasswordView($id){
        $user=User::find($id);
        \Session::put('backUrl',\URL::previous());
        return view('userviews.changePasswordForm')->with('user',$user);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed'
        ],[],[
            'password' => 'Hasło'
        ]);

        if (!(\Hash::check($request->oldPassword, \Auth::user()->password))) { //jeśli hasło jest złe
            $errors = new MessageBag(['oldPassword' => 'Wprowadzone hasło jest niepoprawne']);
//            return redirect('changePasswordView')->withErrors($errors);
            return back()->withErrors($errors);
        } elseif (\Hash::check($request->oldPassword, \Auth::user()->password)) { //jeśli hasło jest dobre
            User::changeUserPassword(\Auth::user()->id, $request->password);
            $url = \Session::get('backUrl');
            return redirect($url)->with('message', 'Twoje hasło zostało zmienione poprawnie');
        } else {
            return redirect('error');
        }
    }

    public function userArticleDetails($id){
        $article=Article::find($id);
        if($article<>null) {
            $articleAttachments = ArticleAttachment::where('articleId', $id);
            $comments = Comment::where('articleId', $id)->get();
            return view('userviews.loggeduserviews.userArticleDetails')->with(['article' => $article, 'articleAttachments' => $articleAttachments, 'comments' => $comments]);
        }
        else{
            return abort(404);
        }
    }

    public function allArticlesView(){
        $articles = Article::where('articleStatus','confirmed')->paginate(6);
        return view('userviews.loggeduserviews.allArticles')->with('lastArticles',$articles);
    }

    public function allArticlesConfirmedView(){
        $lastArticles = Article::where('articleStatus','confirmed')->paginate(6);
        return view('userviews.unloggeduserviews.allArticlesConfirmed')->with(['lastArticles'=>$lastArticles]);
    }

    public function sendContactMail(Request $request){
        $titleMail = $request->title;
        $contentMail = $request->contentMail;
        $userMail = $request->contactEmail;
        $contactEmail = $request->contactEmail;

        $content =[
            'subject'=>$titleMail,
            'text'=>$contentMail,
            'userMail'=>$userMail,
            'contactEmail'=>$contactEmail
        ];

        \Mail::to('superinfoserwis@gmail.com')
            ->send(new ContactMail($content));
        return redirect('/')->with('message','Wiadomość została wysłana pomyślnie');
    }

    public function ourRedactorsView(){
        $redactors = User::where('status','writer')->where('activateStatus',1)->paginate(10);
        return view('userviews.ourRedactors')->with(['redactors'=>$redactors]);
    }

    public function announcements(){
        $lastAnnouncements = Announcement::where('updated_at','>',Carbon::today()->subDays(7))->orderBy('updated_at','desc')->paginate(3);
        return view('userviews.unloggeduserviews.activeAnnouncements')->with('lastAnnouncements',$lastAnnouncements);
    }




}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::guest()){
            return redirect('error');
        }
        if(\Auth::user()->status <> 'admin'){
            return redirect('error');
        }
        return $next($request);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmArticle extends Mailable
{
    use Queueable, SerializesModels;
    public  $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content=$content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.adminInfo')
            ->subject($this->content['subject'])
            ->to($this->content['user']->email, $this->content['user']->name . " " . $this->content['user']->lastName)
            ->with('content',$this->content);
    }
}

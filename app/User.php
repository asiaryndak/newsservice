<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,CanResetPassword;
    protected $primaryKey = 'id';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lastName', 'status', 'activateStatus','created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function createUser($name, $lastName, $password, $status, $email, $activateStatus)
    {
        $newUser = new User(['name' => $name, 'lastName' => $lastName, 'password' => $password, 'status' => $status,
            'email' => $email, 'activateStatus' => $activateStatus]);
        $newUser->save();
    }

    public static function updateProfileData($id, $name, $lastName, $email)
    {
        $updateUser = User::find($id);
        $updateUser->update(['name' => $name, 'lastName' => $lastName, 'email' => $email]);
        $updateUser->save();
        return $updateUser;
    }

    public static function changeUserPassword($id, $newPass)
    {
        $updateUserPass = User::find($id);
        $updateUserPass->password = \Hash::make($newPass);
        $updateUserPass->save();
        return $updateUserPass;
    }


    /**Publiczna funkcja bedaca powiazaniem miedzy tabela uzytkownikow, a tabela artykulow(Article)
     * Kazdy uzytkownik(z odpowiednim statusem) moze miec wiele artykulow
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    /**Publiczna funkcja bedaca powiazaniem miedzy tabela uzytkownikow a tabela ogloszen(announcements)
     * Kazdy uzytkownik moze miec wiele ogloszen
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
        return $this->hasMany('App\Announcement');
    }


}

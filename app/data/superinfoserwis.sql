-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 17 Sty 2018, 15:36
-- Wersja serwera: 10.1.10-MariaDB
-- Wersja PHP: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `superinfoserwis`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `announcements`
--

CREATE TABLE `announcements` (
  `announcementId` int(10) UNSIGNED NOT NULL,
  `announcementTitle` varchar(70) NOT NULL,
  `announcementType` enum('find','lost','forSale','buy') NOT NULL,
  `announcementContent` varchar(350) NOT NULL,
  `contactType` enum('email','phone') NOT NULL,
  `phone` int(10) UNSIGNED DEFAULT NULL,
  `announceAuthorId` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `announcements`
--

INSERT INTO `announcements` (`announcementId`, `announcementTitle`, `announcementType`, `announcementContent`, `contactType`, `phone`, `announceAuthorId`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'SPRZEDAM AUTO', 'forSale', 'Sprzedam auto z rocznika 2000, fiat punto, silnik 1.2. \n            Auto jak na swój wiek zadbane, możliwość obejrzenia po uprzednim kontakcie telefonicznym. \n            Cena do negocjacji', 'phone', 666333555, 2, NULL, '2018-01-15 23:00:00', '2018-01-16 23:00:00'),
(6, 'ZGUBIONO TELEFON', 'forSale', 'W dniu 16.01.2018 w okolicy Uniwersytetu Rzeszowskiego zgubiono telefon LG. \n            Telefon posiada charakterystyczną zieloną obudowę. \n            Znalazca proszony jest o kontakt na maila podanego w ogłoszeniu. Z góry dziękuję :) \n            Cena do negocjacji', 'phone', 666333555, 2, NULL, '2018-01-15 23:00:00', '2018-01-16 23:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles`
--

CREATE TABLE `articles` (
  `articleId` int(10) UNSIGNED NOT NULL,
  `articleTitle` varchar(200) NOT NULL,
  `articleContent` text NOT NULL,
  `articleViewCount` int(11) DEFAULT NULL,
  `articleAuthorId` int(10) UNSIGNED DEFAULT NULL,
  `articleCategory` enum('news','history','curiosity') NOT NULL,
  `articleStatus` enum('waiting','confirmed','rejected','archive') NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles`
--

INSERT INTO `articles` (`articleId`, `articleTitle`, `articleContent`, `articleViewCount`, `articleAuthorId`, `articleCategory`, `articleStatus`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dbaj o swoje zdrowie, nie masz nic cenniejszego od niego!', 'Bardzo często natłok obowiązków, i pęd świata dyktowany chęcią osiągnięcia wszystkiego,\n                konieczności równoważenia życia zawodowego z osobistym, pochłaniają tyle czasu,że zapominamy zupełnie o swoim zdrowiu.\n                Jednak nie należy tak robić, ponieważ prędzej czy później wszelkie zaniedbania odpłacą się nam pięknym za nadobne,\n                a wówczas nic poza zdrowiem nie będzie miało już takiej wartości.', NULL, 1, 'news', 'rejected', NULL, '2017-12-11 23:00:00', '2018-01-17 12:29:04'),
(2, 'Tworzenie serwisu internetowego może być fajne! Przyjdź na warsztaty organizowane przez twórcę serwisu bestNews.pl i przekonaj się jakie to proste', 'Już dziś udowodnij sobie, że nie ma dla Ciebie rzeczy niemożliwych i zapisz się na \n                nasze warsztaty. Tylko przez określony czas będą one bezpłatne, więc takiej szansy nie wolno zmarnować!', NULL, 1, 'news', 'rejected', NULL, '2017-12-11 23:00:00', '2018-01-17 12:30:02'),
(3, 'Rozpoczął się rok 2018. Pora na realizowanie zamierzonych postanowień noworocznych !', 'Czy ty również w tym roku postanowiłeś sobie, że ten czas będzie należał do Ciebie ? Jeśli tak, to nie porzucaj swoich postanowień szybciej niż niezjedzonego sylwestrowego jedzenia! Nie daj się zniechęcić początkowymi trudnościami z osiągnięciem zamierzonych efektów. Wytrwaj dłużej, a osiągniesz nie tylko zamierzony cel, ale i satysfakcję z pokonania własnych słabości.', NULL, 1, '', 'confirmed', NULL, '2017-12-31 23:00:00', '2018-01-17 13:57:24'),
(5, 'Ciekawostka historyczna na dziś.', 'W tym roku obchodzimy okrągłą 100 rocznicę odzyskania przez Polskę niepodległości.\n                Warto w tym dniu zatrzymać się na chwilę i oddać cześć tym, którzy walczyli o ojczyznę.\n                Wywieszenie flagi i chwila zadumy w takim dniu to gest szacunku.', NULL, 1, 'curiosity', 'confirmed', NULL, '2018-01-09 23:00:00', '2018-01-16 23:00:00'),
(6, 'Ruszył nowy serwis informacyjny. Nie zwlekaj i zapoznaj się z nim już dziś!', 'W ramach pracy inżynierskiej tworzonej pod czujnym okiem promotora, powstał\r\n                nowy serwis informacyjny.Umożlwia on nie tylko podstawowe przeglądanie wiadomości,\r\n                ale także zamie<a rel="nofollow" target="_blank" href="http://wp.pl">http://wp.pl/</a> szczanie ogłoszeń, wystawianie własnych przedmiotów, czy też kontakt\r\n                z wystawcami. Szerokie spectrum możliwości w przyszłości zostanie powiększone o kolejne, \r\n                nieukazane dotąd możliwości. Warto zarejestrować się już dziś, i zapewnić sobie stały dostęp do serwisu bestNews.pl', NULL, 1, '', 'confirmed', NULL, '2018-01-15 23:00:00', '2018-01-17 12:33:35'),
(7, 'Masa imprez biegowych przyczynia się do promowania zdrowego stylu życia.', 'Coraz więcej miast decyduje się na organizowanie imprez biegowych,\n                przeznaczając na ten cel środki z budżetu miasta. Część mieszkańców nie kryje swojego\n                niezadowolenia z tego powodu, gdyż uważają, że pieniądze publiczne powinny być inwestowane w \n                jak sami mawiają.. ważniejsze sprawy. Redakcja bestNews.pl nie przytakuje jednak takiemu stanowisku\n                i gorąco zachęca prezydentów, burmistrzów miast do promowania takiego trybu życia.\n                Należy nie zważać na krytykę i robić swoje, bo dogodzić wszystkim jest ciężko, a zdenerwować\n                nietrudno. Jednak nie działając, nic nie osiągniemy, więc trzeba zakasać rękawy i brać się do roboty ! \n                \n                A wy, jakie macie stanowisko w tej sprawie ? Koniecznie napiszcie o tym w komentarzach do tego artykułu, bądź \n                też na adres mailowy redakcji, nadając w tytule tytuł tego artykułu.', NULL, 1, 'news', 'confirmed', NULL, '2018-01-15 23:00:00', '2018-01-17 12:27:55'),
(8, 'aa', '<h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><h3>Fragment 1.10.32 z "de Finibus Bonorum et Malorum", napisanej przez Cycerona w 45 r.p.n.e.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>', NULL, 1, 'news', 'waiting', NULL, '2018-01-17 13:13:25', '2018-01-17 13:13:25');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `article_attachments`
--

CREATE TABLE `article_attachments` (
  `attachmentId` int(10) UNSIGNED NOT NULL,
  `attachmentLink` varchar(191) NOT NULL,
  `attachmentType` enum('image','video') NOT NULL,
  `articleId` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

CREATE TABLE `comments` (
  `commentId` int(10) UNSIGNED NOT NULL,
  `commentText` varchar(255) NOT NULL,
  `articleId` int(10) UNSIGNED DEFAULT NULL,
  `commentAuthorId` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`commentId`, `commentText`, `articleId`, `commentAuthorId`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'To miała być wiadomość news ? Zajmijcie się czymś ważniejszym!', 1, 2, NULL, '2017-12-27 23:00:00', '2017-12-27 23:00:00'),
(3, 'Potwierdzam, o zdrowie trzeba dbać, im wcześniej tym lepiej ! Pozdrawiam redakcję', 1, 4, NULL, '2017-12-27 23:00:00', '2017-12-27 23:00:00'),
(6, 'Kolejny nudny wykład. Czy nie można by zrobić czegoś bardziej wartościowego?', 2, 2, NULL, '2017-12-27 23:00:00', '2017-12-27 23:00:00'),
(9, 'Wy chyba jesteście niepoważni...', 1, 4, NULL, '2017-12-27 23:00:00', '2017-12-27 23:00:00'),
(16, 'Bardzo dobrze prawią!', 7, 2, NULL, '2018-01-17 12:27:21', '2018-01-17 12:27:21');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2017_11_04_151913_create_articles_table', 1),
(10, '2017_11_07_093838_create_articlesAttachments_table', 1),
(11, '2017_11_07_102949_create_comments_table', 1),
(12, '2017_11_07_104720_create_announcements_table', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `lastName` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `status` enum('user','admin','writer','redactor') NOT NULL,
  `email` varchar(191) NOT NULL,
  `activateStatus` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `lastName`, `password`, `status`, `email`, `activateStatus`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Krzysztof', 'Zdun', '$2y$10$HXfgEjWgFcwfbVEiOtPqRO/UCjrE57Ps2fqsWFxCaYATSYC9Yt0hq', 'writer', 'writer@yopmail.com', 1, 'JLvTZLskvHNVsfRQMefG6dYvHLtk9eXijJxXnF3mG1na8NiHWapv3bftUnFZ', '2017-07-27 22:00:00', '2018-01-17 12:51:51'),
(2, 'Jan', 'Nowak', '$2y$10$5AApOjSCUGLRlfYI/s5JTeUJWP/rDUh3Vh6yq48aojg2lBApL/YYq', 'admin', 'admin@admin.pl', 1, '8Wa8R2ZWcAxDDwjtTVqGXDMszX5gUu4xmPckIaYmoRzwynC9F1dHzPPmu0L9', '2017-07-27 22:00:00', '2018-01-17 10:23:25'),
(4, 'Jan', 'Gryziec', '$2y$10$CSt4FW4/NMsuJxpWjvnSGOZ9dQkcnIeaNPmCmhHgIgUSy3U2gLBhG', 'user', 'jgryziec@yopmail.com', 0, NULL, '2017-07-27 22:00:00', '2017-12-27 23:00:00'),
(7, 'Joanna', 'Ryndak', '$2y$10$jVwJ78D3esN40bAqVQx8L.7fm5PdVOzA7D3yXXWMd2tRmjt/9umvi', 'user', 'superinfoserwis@gmail.com', 1, 'p3fdfbQqbntsVptCzzwNWpGgL0WqY1foqUO2cbzhts24fVexgQiS6w7r5vCO', '2018-01-16 23:00:00', '2018-01-17 13:00:18');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`announcementId`),
  ADD KEY `announcements_announceauthorid_foreign` (`announceAuthorId`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`articleId`),
  ADD KEY `articles_articleauthorid_foreign` (`articleAuthorId`);

--
-- Indexes for table `article_attachments`
--
ALTER TABLE `article_attachments`
  ADD PRIMARY KEY (`attachmentId`),
  ADD KEY `article_attachments_articleid_foreign` (`articleId`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`commentId`),
  ADD KEY `comments_articleid_foreign` (`articleId`),
  ADD KEY `comments_commentauthorid_foreign` (`commentAuthorId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `announcements`
--
ALTER TABLE `announcements`
  MODIFY `announcementId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `articles`
--
ALTER TABLE `articles`
  MODIFY `articleId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `article_attachments`
--
ALTER TABLE `article_attachments`
  MODIFY `attachmentId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `comments`
--
ALTER TABLE `comments`
  MODIFY `commentId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT dla tabeli `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `announcements`
--
ALTER TABLE `announcements`
  ADD CONSTRAINT `announcements_announceauthorid_foreign` FOREIGN KEY (`announceAuthorId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_articleauthorid_foreign` FOREIGN KEY (`articleAuthorId`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `article_attachments`
--
ALTER TABLE `article_attachments`
  ADD CONSTRAINT `article_attachments_articleid_foreign` FOREIGN KEY (`articleId`) REFERENCES `articles` (`articleId`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_articleid_foreign` FOREIGN KEY (`articleId`) REFERENCES `articles` (`articleId`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_commentauthorid_foreign` FOREIGN KEY (`commentAuthorId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

DELIMITER $$
--
-- Zdarzenia
--
CREATE DEFINER=`root`@`localhost` EVENT `deleteRejectedArticles` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-17 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE from articles where articleStatus='rejected' AND
TimeStampDiff(DAY, CURRENT_TIMESTAMP,updated_at)>7$$

CREATE DEFINER=`root`@`localhost` EVENT `archiveArticle` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-17 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE articles SET articleStatus ='archive' WHERE articleStatus='confirmed' and TimeStampDiff(DAY, CURRENT_TIMESTAMP,updated_at)>30$$

CREATE DEFINER=`root`@`localhost` EVENT `deleteEndedAnnounce` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-17 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO Delete from announcements WHERE TimeStampDiff(DAY, CURRENT_TIMESTAMP,updated_at)>30$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

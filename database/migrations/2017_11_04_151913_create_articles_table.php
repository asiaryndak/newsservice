<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('articleId');
            $table->string('articleTitle',200);
            $table->text('articleContent');
            $table->integer('articleViewCount')->nullable();
            $table->integer('articleAuthorId')->nullable()->unsigned();
            $table->enum('articleCategory',['news','history','curiosity']);
            $table->enum('articleStatus',['waiting','confirmed','rejected','archive']);
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('articleAuthorId')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}

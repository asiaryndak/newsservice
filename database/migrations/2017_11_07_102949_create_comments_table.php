<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('commentId');
            $table->string('commentText',255);
            $table->integer('articleId')->unsigned()->nullable();
            $table->integer('commentAuthorId')->unsigned()->nullable();
            $table->foreign('articleId')->references('articleId')->on('articles')->onDelete('cascade');
            $table->foreign('commentAuthorId')->references('id')->on('users')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

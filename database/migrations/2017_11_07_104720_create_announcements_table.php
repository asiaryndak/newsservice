<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('announcementId');
            $table->string('announcementTitle',70);
            $table->enum('announcementType',['find','lost','forSale','buy']);
            $table->string('announcementContent',350);
//            $table->dateTime('announcementAddDate');
//            $table->date('announcementFinishDate');
//            $table->integer('announceViewCount')->unsigned();
            $table->enum('contactType',['email','phone']);
            $table->integer('phone')->unsigned()->nullable();
            $table->integer('announceAuthorId')->unsigned()->nullable();
            $table->foreign('announceAuthorId')->references('id')->on('users')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}

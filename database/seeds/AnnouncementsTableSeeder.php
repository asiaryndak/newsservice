<?php

use Illuminate\Database\Seeder;

class AnnouncementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    DB::table('announcements')->insert([
            [
                'announcementTitle' => 'Sprzedam laptopa',
                'announcementType' => 'forSale',
                'announcementContent' => 'Sprzedam laptopa. Kupiony dwa lata temu, okres gwarancyjny trwa jeszcze rok.
                Laptop firmy HP, z procesorem Intel i3 trzeciej generacji. Pamięć operacyjna ram: 500 GB ( Dysk HDD).
                Cena do ustalenia, więcej informacji na maila.',
                'contactType' => 'email',
                'phone' => null,
                'announceAuthorId'=>'1',
                'created_at' => '2017-07-28',
                'updated_at' => '2017-07-28',
            ],
            [
                'announcementTitle' => 'Kupię auto używane',
                'announcementType' => 'buy',
                'announcementContent' => 'Poszukuję auta w cenie do 5000 zł. Istotną sprawą jest by było
                w dobrym stanie technicznym. O resztę informacji proszę pod numerem telefonu.',
                'contactType' => 'phone',
                'phone' => '123456789',
                'announceAuthorId'=>'1',
                'created_at' => '2017-07-28',
                'updated_at' => '2017-07-28',
            ],
            [
                'announcementTitle' => 'Znaleziono klucze',
                'announcementType' => 'find',
                'announcementContent' => 'W okolicy Millenium Hall w Rzeszowie znaleziono klucze. Każdego, kto
                w ostatnim czasie utracił jakieś klucze, proszę o kontakt na email. Przed oddaniem, weryfikacja,
                polegająca na opisaniu zguby.',
                'contactType' => 'email',
                'phone' => null,
                'announceAuthorId'=>'1',
                'created_at' => \Carbon\Carbon::yesterday(),
                'updated_at' => \Carbon\Carbon::today(),
            ],
        [
            'announcementTitle' => 'KUPIĘ MIESZKANIE',
            'announcementType' => 'buy',
            'announcementContent' => 'Kupię mieszkanie w Rzeszowie, do 500 tys. zł. 
            Najlepiej gdzieś na obrzeżach miasta, ale z dobrą komunikacją miejską.',
            'contactType' => 'email',
            'phone' => null,
            'announceAuthorId'=>'2',
            'created_at' => \Carbon\Carbon::yesterday(),
            'updated_at' => \Carbon\Carbon::today(),
        ],
        [
            'announcementTitle' => 'SPRZEDAM AUTO',
            'announcementType' => 'forSale',
            'announcementContent' => 'Sprzedam auto z rocznika 2000, fiat punto, silnik 1.2. 
            Auto jak na swój wiek zadbane, możliwość obejrzenia po uprzednim kontakcie telefonicznym. 
            Cena do negocjacji',
            'contactType' => 'phone',
            'phone' => '666333555',
            'announceAuthorId'=>'2',
            'created_at' => \Carbon\Carbon::yesterday(),
            'updated_at' => \Carbon\Carbon::today(),
        ],
        [
            'announcementTitle' => 'ZGUBIONO TELEFON',
            'announcementType' => 'forSale',
            'announcementContent' => 'W dniu 16.01.2018 w okolicy Uniwersytetu Rzeszowskiego zgubiono telefon LG. 
            Telefon posiada charakterystyczną zieloną obudowę. 
            Znalazca proszony jest o kontakt na maila podanego w ogłoszeniu. Z góry dziękuję :) 
            Cena do negocjacji',
            'contactType' => 'phone',
            'phone' => '666333555',
            'announceAuthorId'=>'2',
            'created_at' => \Carbon\Carbon::yesterday(),
            'updated_at' => \Carbon\Carbon::today(),
        ],
        [
            'announcementTitle' => 'Znaleziono kota',
            'announcementType' => 'find',
            'announcementContent' => 'W dniu 16.01.2018 w okolicy Uniwersytetu Rzeszowskiego znaleziono
            szaro burego kotka. Kot bardzo ufny w stosunku do ludzi. ',
            'contactType' => 'phone',
            'phone' => '666333554',
            'announceAuthorId'=>'3',
            'created_at' => \Carbon\Carbon::yesterday(),
            'updated_at' => \Carbon\Carbon::today(),
        ],


            ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            ['articleTitle'=>'Dbaj o swoje zdrowie, nie masz nic cenniejszego od niego!',
                'articleContent'=>'Bardzo często natłok obowiązków, i pęd świata dyktowany chęcią osiągnięcia wszystkiego,
                konieczności równoważenia życia zawodowego z osobistym, pochłaniają tyle czasu,że zapominamy zupełnie o swoim zdrowiu.
                Jednak nie należy tak robić, ponieważ prędzej czy później wszelkie zaniedbania odpłacą się nam pięknym za nadobne,
                a wówczas nic poza zdrowiem nie będzie miało już takiej wartości.',
                'articleAuthorId'=>'1',
                'articleCategory'=>'news',
                'articleStatus'=>'waiting',
                'created_at' => '2017-12-12',
                'updated_at' => '2017-12-28',
            ],


            ['articleTitle'=>'Tworzenie serwisu internetowego może być fajne! Przyjdź na warsztaty organizowane przez twórcę serwisu bestNews.pl i przekonaj się jakie to proste',
                'articleContent'=>'Już dziś udowodnij sobie, że nie ma dla Ciebie rzeczy niemożliwych i zapisz się na 
                nasze warsztaty. Tylko przez określony czas będą one bezpłatne, więc takiej szansy nie wolno zmarnować!',
                'articleAuthorId'=>'1',
                'articleCategory'=>'news',
                'articleStatus'=>'confirmed',
                'created_at' => '2017-12-12',
                'updated_at' => '2017-12-28',
                ],


            ['articleTitle'=>'Rozpoczął się rok 2018. Pora na realizowanie zamierzonych postanowień noworocznych !',
                'articleContent'=>'Czy ty również w tym roku postanowiłeś sobie, że ten czas będzie należał do Ciebie ?
                Jeśli tak, to nie porzucaj swoich postanowień szybciej niż niezjedzonego sylwestrowego jedzenia! 
                Nie daj się zniechęcić początkowymi trudnościami z osiągnięciem zamierzonych efektów. Wytrwaj
                dłużej, a osiągniesz nie tylko zamierzony cel, ale i satysfakcję z pokonania własnych słabości.',
                'articleAuthorId'=>'1',
                'articleCategory'=>'news',
                'articleStatus'=>'confirmed',
                'created_at' => '2018-01-01',
                'updated_at' => '2018-01-01',
                ],

            ['articleTitle'=>'Ciekawostka historyczna na dziś.',
                'articleContent'=>'W tym roku obchodzimy okrągłą 100 rocznicę odzyskania przez Polskę niepodległości.
                Warto w tym dniu zatrzymać się na chwilę i oddać cześć tym, którzy walczyli o ojczyznę.
                Wywieszenie flagi i chwila zadumy w takim dniu to gest szacunku.',
                'articleAuthorId'=>'1',
                'articleCategory'=>'curiosity',
                'articleStatus'=>'confirmed',
                'created_at' => \Carbon\Carbon::today()->subDays(7),
                'updated_at' => \Carbon\Carbon::today(),
                ],
            ['articleTitle'=>'Ciekawostka historyczna na dziś.',
                'articleContent'=>'W tym roku obchodzimy okrągłą 100 rocznicę odzyskania przez Polskę niepodległości.
                Warto w tym dniu zatrzymać się na chwilę i oddać cześć tym, którzy walczyli o ojczyznę.
                Wywieszenie flagi i chwila zadumy w takim dniu to gest szacunku.',
                'articleAuthorId'=>'1',
                'articleCategory'=>'curiosity',
                'articleStatus'=>'confirmed',
                'created_at' => \Carbon\Carbon::today()->subDays(7),
                'updated_at' => \Carbon\Carbon::today(),
            ],

            ['articleTitle'=>'Ruszył nowy serwis informacyjny. Nie zwlekaj i zapoznaj się z nim już dziś!',
                'articleContent'=>'W ramach pracy inżynierskiej tworzonej pod czujnym okiem promotora, powstał
                nowy serwis informacyjny.Umożlwia on nie tylko podstawowe przeglądanie wiadomości,
                ale także zamieszczanie ogłoszeń, wystawianie własnych przedmiotów, czy też kontakt
                z wystawcami. Szerokie spectrum możliwości w przyszłości zostanie powiększone o kolejne, 
                nieukazane dotąd możliwości. Warto zarejestrować się już dziś, i zapewnić sobie stały dostęp do serwisu bestNews.pl',
                'articleAuthorId'=>'1',
                'articleCategory'=>'news',
                'articleStatus'=>'confirmed',
                'created_at' => \Carbon\Carbon::yesterday(),
                'updated_at' => \Carbon\Carbon::today(),
                ],

            ['articleTitle'=>'Masa imprez biegowych przyczynia się do promowania zdrowego stylu życia.',
                'articleContent'=>'Coraz więcej miast decyduje się na organizowanie imprez biegowych,
                przeznaczając na ten cel środki z budżetu miasta. Część mieszkańców nie kryje swojego
                niezadowolenia z tego powodu, gdyż uważają, że pieniądze publiczne powinny być inwestowane w 
                jak sami mawiają.. ważniejsze sprawy. Redakcja bestNews.pl nie przytakuje jednak takiemu stanowisku
                i gorąco zachęca prezydentów, burmistrzów miast do promowania takiego trybu życia.
                Należy nie zważać na krytykę i robić swoje, bo dogodzić wszystkim jest ciężko, a zdenerwować
                nietrudno. Jednak nie działając, nic nie osiągniemy, więc trzeba zakasać rękawy i brać się do roboty ! 
                
                A wy, jakie macie stanowisko w tej sprawie ? Koniecznie napiszcie o tym w komentarzach do tego artykułu, bądź 
                też na adres mailowy redakcji, nadając w tytule tytuł tego artykułu.',


                'articleAuthorId'=>'1',
                'articleCategory'=>'news',
                'articleStatus'=>'waiting',
                'created_at' => \Carbon\Carbon::yesterday(),
                'updated_at' => \Carbon\Carbon::today(),
            ],

            ]);
    }
}

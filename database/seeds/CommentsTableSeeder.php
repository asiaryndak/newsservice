<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            [
                'commentText' => 'Bardzo fajny artykuł. Tak trzymać redakcjo!',
                'articleId' => '1',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'To miała być wiadomość news ? Zajmijcie się czymś ważniejszym!',
                'articleId' => '1',
                'commentAuthorId'=>'2',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Potwierdzam, o zdrowie trzeba dbać, im wcześniej tym lepiej ! Pozdrawiam redakcję',
                'articleId' => '1',
                'commentAuthorId'=>'4',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Bardzo fajny artykuł. Tak trzymać redakcjo!',
                'articleId' => '2',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Już się zapisałem, i nie mogę się doczekać !',
                'articleId' => '2',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Kolejny nudny wykład. Czy nie można by zrobić czegoś bardziej wartościowego?',
                'articleId' => '2',
                'commentAuthorId'=>'2',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Bardzo fajny artykuł. Tak trzymać redakcjo!',
                'articleId' => '3',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Co to za artykuł?!',
                'articleId' => '1',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Wy chyba jesteście niepoważni...',
                'articleId' => '1',
                'commentAuthorId'=>'4',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Jak można zacząć tworzyć takie artykuły?',
                'articleId' => '1',
                'commentAuthorId'=>'2',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Bardzo fajny artykuł. Tak trzymać redakcjo!',
                'articleId' => '1',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Bardzo fajny artykuł. Tak trzymać redakcjo!',
                'articleId' => '1',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'commentText' => 'Bardzo fajny artykuł. Tak trzymać redakcjo!',
                'articleId' => '1',
                'commentAuthorId'=>'3',
                'created_at' => '2017-12-28',
                'updated_at' => '2017-12-28',
            ],
            ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                'name' => 'Krzysztof',
                'lastName' => 'Zdun',
                'password' => bcrypt('writer'),
                'status' => 'writer',
                'email' => 'writer@yopmail.com',
//                'activateStatus'=>'true',
                'created_at' => '2017-07-28',
                'updated_at' => '2017-07-28',
            ],

            [
                'name' => 'Zbigniew',
                'lastName' => 'Nowak',
                'password' => bcrypt('admin'),
                'status' => 'admin',
                'email' => 'admin@admin.pl',
//                'activateStatus'=>'1',
                'created_at' => '2017-07-28',
                'updated_at' => '2017-07-28',
            ],
            [
                'name' => 'Anna',
                'lastName' => 'Iksińska',
                'password' => bcrypt('aiksinska'),
                'status' => 'writer',
                'email' => 'aiksinska@yopmail.com',
//                'activateStatus'=>'true',
                'created_at' => '2017-12-12',
                'updated_at' => '2017-12-28',
            ],
            [
                'name' => 'Jan',
                'lastName' => 'Gryziec',
                'password' => bcrypt('jgryziec'),
                'status' => 'user',
                'email' => 'jgryziec@yopmail.com',
//                'activateStatus'=>'true',
                'created_at' => '2017-07-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'name' => 'Kasia',
                'lastName' => 'Burda',
                'password' => bcrypt('kburda'),
                'status' => 'user',
                'email' => 'kburda@yopmail.com',
//                'activateStatus'=>'true',
                'created_at' => '2017-07-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'name' => 'Mirosław',
                'lastName' => 'Wójcik',
                'password' => bcrypt('mwojcik'),
                'status' => 'user',
                'email' => 'mwojcik@yopmail.com',
//                'activateStatus'=>'true',
                'created_at' => '2018-01-28',
                'updated_at' => '2017-12-28',
            ],
            [
                'name' => 'Joanna',
                'lastName' => 'Ryndak',
                'password' => bcrypt('jryndak'),
                'status' => 'user',
                'email' => 'superinfoserwis@gmail.com',
//                'activateStatus'=>'true',
                'created_at' => \Carbon\Carbon::today(),
                'updated_at' => '2017-12-28',
            ],
            ]);
    }
}

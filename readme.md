**Projekt internetowego serwisu informacyjnego w języku PHP przy wykorzystaniu frameworku Laravel.**

KRÓTKI OPIS
-
Projekt przygotowany jako projekt uczelniany. Nie został dopracowany, zaimplementowane zostały jedynie podstawowe funkcjonalności. Projekt był próbą zapoznania się z podstawami frameworku Laravel. Do implementacji zostały wykorzystane także elementy Bootstrapa. W projekcie użyto bazy MySQL. 

Projekt z założenia miał być bardzo prostym serwisem oferującym podstawowe funkcjonalności, jak przeglądanie wiadomości, czy też ogłoszeń dodawanych przez użytkowników serwisu. Rozróżnione zostało kilka typów kont użytkowników, chociaż samo podstawowe korzystanie z serwisu jest dostępne bez logowania. 
- **Użytkownik niezalogowany** - podstawowy typ, umożliwia jedynie przeglądanie wiadomości, ogłoszeń, bez możliwości komentowania, czy dodawania własnych treści
- **Użytkownik zalogowany - user** - konto użytkownika zalogowanego obejmujące najmniejsze spektrum możliwości - tj. dodawanie ogłoszeń, komentowanie informacji,
- **Użytkownik zalogowany - redaktor** - konto umożliwiające funkcjonalności niższych szczebli, a dodatkowo zezwalające na redagowanie własnych tekstów wiadomości,
- **Użytkownik zalogowany - administrator** - konto posiadające własności kont niższych szczebli, a dodatkowo posiadające opcję zarządzania zarówno użytkownikami jak i tekstami przezeń tworzonymi

**URUCHAMIANIE:**
- 

- Aby uruchomić projekt należy mieć zainstalowane : PHP, Composer, oraz np. XAMPP.
- Znajdując się w folderze projektu należy w wierszu poleceń wywołać komendę : _composer install_
- Następnie należy wygenerować klucz, przy użyciu polecenia : _php artisan key:generate_
- Stworzyć pustą bazę MySQL, oraz skonfigurować dane połączenia w pliku .env
- Wczytać gotowe dane do bazy ze skryptu znajdującego się w folderze data, bądź też uruchomić inicjalizację bazy z poziomu projektu wywołaniem polecenia: _php artisan migrate_, oraz _php artisan db:seed_
- Należy uruchomić projekt poleceniem : _php artisan serve_ - powinien on uruchomić się na domyślnym adresie : http://127.0.0.1:8000 .

Przykładowe dane do logowania można znaleźć w seederach do tabeli użytkowników.
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Podałeś złe dane logowania',
    'throttle' => 'Zbyt dużo złych prób. Spróbuj ponownie za :seconds seconds.',

];

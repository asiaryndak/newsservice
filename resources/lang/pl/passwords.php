<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi mieć przynajmniej 6 znaków i być zgodne z potwierdzeniem.',
    'reset' => 'Twoje hasło zostało pomyślnie zresetowane!',
    'sent' => 'Wysłaliśmy na twoją pocztę e-maila z linkiem do resetowania hasła!',
    'token' => 'Token resetowania hasła jest nieprawidłowy.',
    'user' => "Nie znaleziono użytkownika o takim adresie e-mail.",

];

<?php

/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 11:58
 */
?>

@extends('master')

@section('content')
    <div class="container">
        <div class="jumbotron" style="background: none;padding: 0 2em">
            <div class="col-xs-12 table-bordered users text-center">
                <p><span class="glyphicon glyphicon-user"></span> W naszym serwisie aktualnie zarejestrowanych jest już aż <b>{{$allUsersCount}}</b> użytkowników!</p>
            </div>
{{--Najwieksza ilosc zatwierdzonych artykulow w ostatnim miesiacu--}}
            <div class="table-bordered">
                @foreach($mostArticlesOneRedactorLastMonth as $m)
                    @php($author = \App\User::find($m->articleAuthorId))

                        @endforeach
                        <div class="col-xs-6 pull-left">

                            <div class="table-responsive panel-heading bg-info">
                                <h3 class="text-justify text-info"><span class="glyphicon glyphicon-star" style="color: gold"></span> Najwięcej zatwierdzonych artykułów w ostatnim
                                    miesiącu dodał : <b><a href="/showUserDetails/{{$author->id}}">{{$author->name}} {{$author->lastName}}</a></b> </h3>

                                <table class="table">
                                    <tr>
                                        <th>Tytuł artykułu</th>
                                        <th>Treść</th>
                                        <th>Ilość komentarzy</th>
                                    </tr>
                                    @foreach($mostArticlesOneRedactorLastMonth as $m)
                                        @php($commentsCount = \App\Comment::where('articleId',$m->articleId)->count())
                                            <tr id="{{$m->articleId}}" onclick='routing("/articleDetails/{{$m->articleId}}")' onmouseover = "changeColor({{ $m->articleId }})" onmouseout="mouseOut({{ $m->articleId }})">
                                                <td> {{str_limit($m->articleTitle,15)}}</td>
                                                <td>{!! str_limit($m->articleContent, 25) !!}</td>
                                                <td>{{$commentsCount}}</td>
                                            </tr>
                                            @endforeach
                                </table>

                                <div class="text-center">{{$mostArticlesOneRedactorLastMonth->appends('users', \Illuminate\Support\Facades\Input::get('users',1))->appends('articlesWaiting', \Illuminate\Support\Facades\Input::get('articlesWaiting',1))->appends('lastAddComments', \Illuminate\Support\Facades\Input::get('lastAddComments',1))->links()}}</div>
                                {{--<div class="text-center">{{$mostArticlesOneRedactorLastMonth->links()}}</div>--}}
                            </div>
                            <hr>
                        </div>
            </div>


            <hr>
            {{--Uzytkownicy zarejestrowani w ostatnim tygodniu--}}
            <div class="col-xs-6 pull-right">
                <div class="table-responsive panel-heading bg-warning">
                    <h3 class="text-justify text-info"><span class="glyphicon glyphicon-calendar"></span> W ostatnim tygodniu dołączyli do nas:</h3>

                    <table class="table">
                        <tr>
                            <th>Imię</th>
                            <th>Nazwisko</th>
                            <th>Email</th>
                            <th>Status</th>
                        </tr>
                        @foreach($usersRegistratedLastWeek as $usersLastWeek)
                            <tr id="{{$usersLastWeek->id}}" onclick='routing("/showUserDetails/{{$usersLastWeek->id}}")' onmouseover = "changeColor({{ $usersLastWeek->id }})" onmouseout="mouseOut({{ $usersLastWeek->id }})">
                                <td> {{$usersLastWeek->name}}</td>
                                <td>{{$usersLastWeek->lastName}}</td>
                                <td>{{$usersLastWeek->email}}</td>
                                @if($usersLastWeek->status=='user')
                                    <td>Użytkownik</td>
                                @elseif($usersLastWeek->status=='writer')
                                    <td>Redaktor</td>
                                @elseif($usersLastWeek->status=='admin')
                                    <td>Administrator</td>
                                @endif
                            </tr>
                        @endforeach
                    </table>

                    <div class="text-center">{{$usersRegistratedLastWeek->appends('articles', \Illuminate\Support\Facades\Input::get('articles',1))->appends('articlesWaiting', \Illuminate\Support\Facades\Input::get('articlesWaiting',1))->appends('lastAddComments', \Illuminate\Support\Facades\Input::get('lastAddComments',1))->links()}}</div>
                    {{--<div class="text-center">{{$usersRegistratedLastWeek->links()}}</div>--}}
                </div>
                <hr>

                <div class="list-group text-center">
                    <a href="/showLastRegisteredUsers" class="list-group-item list-group-item-success">Zobacz ostatnio
                        zarejestrowanych użytkowników</a>
                    <a href="/showLastAddedArticles" class="list-group-item list-group-item-info">Zobacz najnowsze
                        artykuły</a>
                    <a href="/showLastAddAnnouncements" class="list-group-item list-group-item-warning">Zobacz ostatnie
                        ogłoszenia</a>
                    <a href="/serviceUsers" class="list-group-item list-group-item-text">Użytkownicy serwisu</a>
                    <a href="/manageArticles" class="list-group-item list-group-item-danger">Artykuły</a>{{--
                    <a href="/manageComments" class="list-group-item list-group-item-success">Komentarze</a>
                    <a href="/manageAnnouncements" class="list-group-item list-group-item-info">Ogłoszenia</a>--}}
                </div>
            </div>
                {{--<hr>--}}

            {{--Artykuly oczekujace na zatwierdzenie--}}
            <div class="table-bordered">
                @foreach($articlesWaiting as $articleWaiting)
                    @php($author = \App\User::find($articleWaiting->articleAuthorId))

                        @endforeach
                        <div class="col-xs-6 pull-left">

                            <div class="table-responsive panel-heading bg-success">
                                <h3 class="text-justify text-info"><span class="glyphicon glyphicon-time"></span> Artykuły oczekujące na zatwierdzenie:</b> </h3>

                                <table class="table">
                                    <tr>
                                        <th>Tytuł artykułu</th>
                                        <th>Treść</th>
                                        <th>Data dodania</th>
                                    </tr>
                                    @foreach($articlesWaiting as $m)
                                            <tr id="{{$m->articleId}}" onclick='routing("/articleDetails/{{$m->articleId}}")' onmouseover = "changeColor({{ $m->articleId }})" onmouseout="mouseOut({{ $m->articleId }})">
                                                <td>{{$m->articleTitle}}</td>
                                            <td>{!! str_limit($m->articleContent, 50) !!}</td>
                                            <td>{{$m->created_at}}</td>

                                            </tr>
                                            @endforeach
                                </table>

                                <div class="text-center">{{$articlesWaiting->appends('users', \Illuminate\Support\Facades\Input::get('users',1))->appends('articles', \Illuminate\Support\Facades\Input::get('articles',1))->appends('lastAddComments', \Illuminate\Support\Facades\Input::get('lastAddComments',1))->links()}}</div>
                                {{--<div class="text-center">{{$mostArticlesOneRedactorLastMonth->links()}}</div>--}}
                            </div>
                            <hr>
                        </div>
            </div>

{{--<hr>--}}

            {{--Ostatnio skomentowane artykuły--}}
                <div class="table-bordered">

                            <div class="col-xs-6 pull-left">
                                <hr>
                                <div class="table-responsive panel-heading bg-pink">
                                    <h3 class="text-justify text-info"><span class="glyphicon glyphicon-pencil"></span> Ostatnio komentowane artykuły:</b> </h3>

                                    <table class="table">
                                        <tr>
                                            <th>Tytuł artykułu</th>
                                            <th>Treść</th>
                                            <th>Data dodania</th>
                                        </tr>
                                        @foreach($lastAddComments as $lac)
                                            @php($article=\App\Article::find($lac->articleId))
                                            <tr id="{{$article->articleId}}" onclick='routing("/articleDetails/{{$article->articleId}}")' onmouseover = "changeColor({{ $article->articleId }})" onmouseout="mouseOut({{ $article->articleId }})">
                                                <td> {{$article->articleTitle}}</td>
                                                <td>{!! str_limit($article->articleContent, 50) !!}</td>
                                                <td>{{$article->created_at}}</td>

                                            </tr>
                                        @endforeach
                                    </table>

                                    <div class="text-center">{{$lastAddComments->appends('users', \Illuminate\Support\Facades\Input::get('users',1))->appends('articles', \Illuminate\Support\Facades\Input::get('articles',1))->appends('articlesWaiting', \Illuminate\Support\Facades\Input::get('articlesWaiting',1))->links()}}</div>
                                </div>
                                <hr>
                            </div>
                </div>






        </div>
        @endsection


        <script type="text/javascript">
            function routing(url) {
                window.location.href = url;
            }
        </script>

        <script>
            function changeColor(name) {
                document.getElementById(name).style.color = "green";
//                document.getElementById(name).style.fontWeight = "bold";
            }

            function mouseOut(name) {
                document.getElementById(name).style.color = "black";
//                document.getElementById(name).style.fontWeight = "normal";

            }
        </script>
    </div>



@section('footer')

@endsection

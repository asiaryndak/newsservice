<?php

/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 11:58
 */
?>

@extends('master')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <div class="table-responsive panel-heading">
                <h3 class="text-justify text-info">Ostatnio zarejestrowani w serwisie użytkownicy</h3>
                <table class="table">
                    <tr>
                        <th>Imię</th>
                        <th>Nazwisko</th>
                        <th>Mail</th>
                        <th>Status</th>
                        <th>Data rejestracji</th>
                    </tr>
                    @foreach($users as $user)
                        @if($user->id!=Auth::user()->id)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->lastName}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->status}}</td>
                            <td>{{$user->created_at}}</td>
                            <td>
                                @if($user->activateStatus==0)
                                    <a href="/makeUserAccountActive/{{$user->id}}">
                                        <button class="btn btn-success" name="activateButton" >
                                            <span class="glyphicon glyphicon-ok"></span> Aktywuj
                                        </button>
                                    </a>
                                @else
                                    <a href="/makeUserAccountDeactivate/{{$user->id}}">
                                        <button class="btn btn-default" name="deactivateButton">
                                            <span class="glyphicon glyphicon-unchecked"></span> Dezaktywuj
                                        </button>
                                    </a>
                                @endif
                                <a href="/showUserActivity/{{$user->id}}">
                                    <button class="btn btn-primary" name="activityButton"><span
                                                class="glyphicon glyphicon-open"></span> Zobacz aktywność
                                    </button>
                                </a>
                                <a href="/editUser/{{$user->id}}">
                                    <button class="btn btn-warning" name="editUserButton"><span
                                                class="glyphicon glyphicon-edit"></span> Edytuj
                                    </button>
                                </a>
                                <a href="/deleteUser/{{$user->id}}">
                                    <button class="btn btn-danger" name="deleteUserButton"><span
                                                class="glyphicon glyphicon-remove"></span> Usuń
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </table>
                <div class="text-center"> {{$users->links()}}</div>
            </div>
        </div>
    </div>
@endsection







@section('footer')

@endsection

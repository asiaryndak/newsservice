<?php

/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 11:58
 */
?>

@extends('master')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <div class="alert-danger text-justify">
                @php($articleAuthor = \App\User::find($article->articleAuthorId))
                <h2>Uwaga! Zamierzasz usunąć artykuł użytkownika {{$articleAuthor->name}} {{$articleAuthor->lastName}} o tytule "{{$article->articleTitle}}"</h2>
                    <h3>Operacja ta jest nieodwracalna. Na pewno chcesz usunąć ten artykuł? </h3>
                <div class="col-lg-6 col-lg-offset-5 col-md-6 col-md-offset-5">
                    <a href="/deleteArticleConfirm/{{$article->articleId}}"><button class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-trash"></span> Usuń</button> </a>
                    <a href="/deleteArticleCancel"><button class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-remove"></span> Anuluj</button> </a>
                </div>
            </div>
        </div>
    </div>
@endsection







@section('footer')

@endsection

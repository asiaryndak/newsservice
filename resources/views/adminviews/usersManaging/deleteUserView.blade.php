<?php

/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 11:58
 */
?>

@extends('master')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <div class="alert-danger text-justify">
                <h2>Uwaga! Zamierzasz usunąć użytkownika {{$user->name}} {{$user->lastName}}</h2>
                    <h3>Operacja ta jest nieodwracalna. Na pewno chcesz usunąć tego użytkownika? </h3>
                <div class="col-lg-6 col-lg-offset-5 col-md-6 col-md-offset-5">
                    <a href="/deleteUserConfirm/{{$user->id}}"><button class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-trash"></span> Usuń</button> </a>
                    <a href="/deleteUserCancel"><button class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-remove"></span> Anuluj</button> </a>
                </div>
            </div>
        </div>
    </div>
@endsection







@section('footer')

@endsection

<?php

/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 11:58
 */
?>

@extends('master')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <div class="list-group text-center">
                {{Form::open(['url'=>'/editUserSave'])}}
                {!! Form::label('uname','Imię',array('class'=>'center-block input-lg text-center')) !!}
                {!! Form::text('name',$user->name,array('id'=>'name','required'=>'required', 'class'=>'center-block input-lg','maxlength' => 100)) !!}<hr>
                {!! Form::label('uLname','Nazwisko',array('class'=>'center-block input-lg text-center')) !!}
                {!! Form::text('lastName',$user->lastName,array('id'=>'name','required'=>'required', 'class'=>'center-block input-lg','maxlength' => 100)) !!}<hr>
                {!! Form::hidden('userId',$user->id,array('id'=>'userId')) !!}
                {!! Form::label('emailLabel','Adres e-mail',array('class'=>'center-block input-lg text-center')) !!}
                {!! Form::email('email',$user->email,array('id'=>'email','required'=>'required', 'class'=>'center-block input-lg', 'autofocus')) !!}<hr>
                {!! Form::label('statusLabel','Status konta',array('class'=>'center-block input-lg text-center','maxlength' => 100))!!}
                {!! Form::select('status',['user'=>'Użytkownik','writer'=>'Redaktor','admin'=>'Administrator'],$user->status,array('class'=>'center-block input-lg text-center','required'=>'required')) !!}
                {!! Form::submit('Zapisz zmiany w profilu',['class'=>'btn btn-success pull-right btn-lg']) !!}
                <a href="{{URL::previous()}}">{!! Form::button('Powróć bez zapisywania zmian',['class'=>'btn btn-warning btn-lg pull-left']) !!}</a>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection







@section('footer')

@endsection

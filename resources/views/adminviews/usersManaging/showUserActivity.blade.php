<?php

/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 11:58
 */
?>

@extends('master')

@section('content')
    <div class="container">
        <div class="jumbotron">
            <div class="list-group text-center">
                <div class="label label-info text-center text-uppercase col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3>
                        Aktywność użytkownika {{$user->name}} {{$user->lastName}} w serwisie bestNews.pl</h3></div>
                <div class="articles">
                    <div class="table-responsive panel-heading">
                        <h3 class="text-justify text-info">Artykuły</h3>
                        <table class="table">
                            <tr>
                                <th>Tytuł</th>
                                <th>Zawartość</th>
                                <th>Kategoria</th>
                                <th>Status artykułu</th>
                            </tr>
                            @foreach($articles as $article)
                                <tr>
                                    <td>{{$article->articleTitle}}</td>
                                    <td>{!! str_limit($article->articleContent, 50) !!}</td>
                                    @if($article->articleCategory=='news')
                                    <td>{{$article->articleCategory}}</td>
                                    @elseif($article->articleCategory=='history')
                                        <td>Historia</td>
                                    @else
                                        <td>Ciekawostka</td>
                                    @endif
                                    @if($article->articleStatus=='waiting')
                                    <td>Oczekujący</td>
                                    @elseif($article->articleStatus=='confirmed')
                                        <td>Zatwierdzony</td>
                                    @elseif($article->articleStatus=='rejected')
                                        <td>Odrzucony</td>
                                    @else
                                        <td>Zarchiwizowany</td>
                                    @endif
                                        <td>
                                        <a href="/articleDetails/{{$article->articleId}}"><button class="btn btn-sm btn-info">Szczegóły</button> </a>
                                        @if($article->articleStatus=='confirmed')
                                            <a href="/rejectArticle/{{$article->articleId}}"><button class="btn btn-sm btn-warning">Odrzuć</button> </a>
                                            <a href="/archiveArticle/{{$article->articleId}}"><button class="btn btn-sm btn-primary">Archwizuj</button> </a>
                                        @elseif($article->articleStatus=='waiting')
                                            <a href="/confirmArticle/{{$article->articleId}}"><button class="btn btn-sm btn-success">Zatwierdź</button> </a>
                                            <a href="/rejectArticle/{{$article->articleId}}"><button class="btn btn-sm btn-warning">Odrzuć</button> </a>
                                            <a href="/archiveArticle/{{$article->articleId}}"><button class="btn btn-sm btn-primary">Archwizuj</button> </a>
                                        @elseif($article->articleStatus=='rejected')
                                            <a href="/archiveArticle/{{$article->articleId}}"><button class="btn btn-sm btn-primary">Archwizuj</button> </a>
                                        @elseif($article->articleStatus=='archive')
                                            <a href="/unarchiveArticle/{{$article->articleId}}"><button class="btn btn-sm btn-warning">Przywróć</button> </a>
                                        @endif
                                        <a href="/deleteArticle/{{$article->articleId}}"><button class="btn btn-sm btn-danger">Usuń</button> </a>
                                    </td>

                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
                <div class="announcements">
                    <div class="table-responsive panel-heading">
                        <h3 class="text-justify text-info">Ogłoszenia</h3>
                        <table class="table">
                            <tr>
                                <th>Tytuł</th>
                                <th>Typ</th>
                                <th>Treść</th>
                                <th>Forma kontaktu</th>
                                <th>Telefon</th>
                            </tr>
                            @foreach($announcements as $announcement)
                                <tr>
                                    <td>{{$announcement->announcementTitle}}</td>
                                    @if($announcement->announcementType=='buy')
                                    <td>Kupię</td>
                                    @elseif($announcement->announcementType=='sold')
                                    <td>Sprzedam</td>
                                    @elseif($announcement->announcementType=='find')
                                    <td>Znalezione</td>
                                    @else
                                    <td>Zgubione</td>
                                    @endif
                                    <td>{!! str_limit($announcement->announcementContent, 50) !!}</td>
                                    @if($announcement->contactType=='email')
                                    <td>{{$announcement->contactType}}</td>
                                    @else
                                        <td>Telefon</td>
                                    @endif
                                    <td>{{$announcement->phone}}</td>
                                    <td>
                                        <a href="/announcementDetails/{{$announcement->announcementId}}"><button class="btn btn-success">Szczegóły</button></a>
                                        <a href="/deleteAnnouncement/{{$announcement->announcementId}}"><button class="btn btn-danger">Usuń</button></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="comments">
                    <div class="table-responsive panel-heading">
                        <h3 class="text-justify text-info">Komentarze</h3>
                        <table class="table">
                            <tr>
                                <th>Treść komentarza</th>
                                <th>Data dodania</th>
                                <th>Tytuł komentowanego artykułu</th>
                            </tr>
                            @foreach($comments as $comment)
                                <tr>
                                    <td>{!! str_limit($comment->commentText, 50) !!}</td>
                                    <td>{{$comment->created_at}}</td>
                                    @php($article=\App\Article::where('articleId',$comment->articleId)->get())
                                        @foreach($article as $a)
                                            <td>{{$a->articleTitle}}</td>
                                            <td><a href="/articleDetails/{{$a->articleId}}"><button class="btn btn-info">Szczegóły</button> </a></td>
                                        @endforeach
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection







@section('footer')

@endsection

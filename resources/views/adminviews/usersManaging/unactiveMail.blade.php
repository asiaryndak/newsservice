<h2>Witaj {{$user->name}} {{$user->lastName}}.</h2>
<h4>Dostajesz tego maila ponieważ administrator serwisu bestNews.pl dezaktywował Twoje konto.</h4>
<h4>Prawdopodobnie jest to wynik działań niezgodnych z regulaminem, podjętych na Twoim koncie w naszym serwisie.</h4>
<h4>Jeśli jednak uważasz, że nie miał ku temu powodów, skontaktuj się z nim poprzez odpowiedź na nasz adres mailowy.</h4>

<h5>Pozdrawiamy, zespół bestNews.pl</h5>
@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body" >

                    @if(Auth::user()->status=='admin')
                    <div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                        <h3 class="text-uppercase text-center">Dane użytkownika {{$user->name}} {{$user->lastName}}</h3>
                        <div class="jumbotron col-lg-12 col-md-12 ">
                            <div class="col-md-12  col-lg-12 " style="background: white">
                                <label class="text-uppercase" style="font-size:x-large;">E-mail: {{$user->email}}</label><hr>
                                <label class="text-uppercase" style="font-size:x-large;">Status: {{$user->status}}</label><hr>
                                <label class="text-uppercase" style="font-size:x-large;">W naszym serwisie od: {{$user->created_at}}</label><hr>
                                @if($user->status=='writer')
                                    @php($articlesCount = \App\Article::where('articleAuthorId',$user->id)->count())
                                        @if($articlesCount>0)
                                    <label class="text-uppercase" style="font-size:x-large;">Jest autorem {{$articlesCount}} artykułów</label>
                                            <a href="/showUserActivity/{{$user->id}}"><button class="btn btn-link center-block">Zobacz jego artykuły</button></a>
                                            @else
                                            <label class="text-uppercase" style="font-size:x-large;">Nie dodał jeszcze żadnego artykułu.</label>
                                            @endif
                                    @endif
                            </div>
                        </div>
                        <a href="/editUser/{{$user->id}}"><button class="btn btn-lg btn-primary center-block">Edytuj jego dane</button></a>
                        <a href="/showUserActivity/{{$user->id}}"><button class="btn btn-lg btn-primary center-block">Pokaż jego aktywność</button></a>
                        {{--<a href="/changePassword/{{$user->id}}"><button class="btn btn-lg btn-primary center-block">Zmień hasło</button></a>--}}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



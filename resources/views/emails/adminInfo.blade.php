@component('mail::message')
# Witaj {{$content['user']->name}} {{$content['user']->lastName}} !

{{--Administrator serwisu bestNews.pl zaakceptował Twój artykuł.--}}

@component('mail::panel')
    {{$content['text']}}
    @endcomponent

Pozdrawiamy, redakcja<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')



@component('mail::panel')
    {{$content['text']}}
    @endcomponent


@component('mail::button', ['url' => 'mailto:'.$content['userMail'].
           '?subject=Odpowiedź na kontakt o temacie:  '.$content['subject'].
            '&body=%0A%0A%0AOdpowiedź na kontakt z  treścią:%0A"'.$content['text'].'"'])
    Odpowiedz
@endcomponent

Pozdrawiamy, redakcja<br>
{{ config('app.name') }}
@endcomponent

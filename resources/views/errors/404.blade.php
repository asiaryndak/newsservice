
@php(header( "refresh:10;url=/" ))
    @extends('master')

    @section('content')
        <div class="full-height jumbotron bg-warning">
    <h1 class="text-danger" style="font-family: 'Source Code Pro'">Błędny adres lub brak uprawnień do oglądania tej strony(HTTP 404)</h1>
            <div class="center-block">
                <h3> Jeśli uważasz, że to pomyłka, zgłoś to do nas poprzez formularz kontaktowy.</h3>
            </div>
            <h4 id="timer" class="pull-right">Przekierowanie na stronę główną nastąpi w ciągu 10 sekund... </h4><br><br>
            <a href="/"><button class="btn btn-info btn-lg pull-right"><span class="glyphicon glyphicon-home"></span> Przejdź teraz</button> </a>

        </div>
@endsection


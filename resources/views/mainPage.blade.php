<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @section('head')
@endsection


    </head>
    <body>

    @extends('master')
    @if(Session::has('message'))
        <div class="alert alert-dismissible alert-success">
            <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
        </div>

        <hr>
    @endif
    @if (!(Auth::guest()))
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body" style="text-align: right">
                            {{--@if (!(Auth::guest()))--}}
                            Witaj {{ Auth::user()->name }} !

                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Wyloguj
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                    </div>

                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body" style="text-align: right">
                         <form><a href="login">Zaloguj się</a></form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

    <div id="carousel-example-generic2" class="carousel slide" style="max-height: 200px">
        <!-- Wskaźniki w postaci kropek -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic2" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic2" data-slide-to="2"></li>
        </ol>

        <!-- Slajdy -->
        <div class="carousel-inner" style="max-height: 200px">
            <div class="item active" style="max-height: 200px">
                <img src="http://placehold.it/1280x500" alt="">
                <!-- Opis slajdu -->
                <div class="carousel-caption" >
                    <h3>To jest opis</h3>
                    <p>pierwszego slajdu</p>
                </div>
            </div>

            <div class="item" style="max-height: 200px">
                <img src="http://placehold.it/1280x500" alt="">
                <!-- Opis slajdu -->
                <div class="carousel-caption">
                    <h3>To jest opis</h3>
                    <p>drugiego slajdu</p>
                </div>
            </div>

            <div class="item" style="max-height: 200px">
                <img src="http://placehold.it/1280x500" alt="">
                <!-- Opis slajdu -->
                <div class="carousel-caption">
                    <h3>To jest opis</h3>
                    <p>trzeciego slajdu</p>
                </div>
            </div>

        </div>

        <!-- Strzałki do przewijania -->
        <a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </div>


    @section('content')

    @endsection


    <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">

                </div>


            </div>
        </div>
    </body>
</html>
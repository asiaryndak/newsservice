<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @yield('head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>bestNews.pl</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}" />
    <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/sidebar-menu-script.js') }}"></script>

</head>
<body>

<nav class="navbar navbar-default" >

    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home"></span> bestNews.pl</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
            <ul class="nav navbar-nav">
                @if(Auth::guest())
                    <li class="{{(Request::is('/allArticlesConfirmed') ? 'active' : '')}}"><a href="/allArticlesConfirmed">Wiadomości<span class="sr-only"></span></a></li>
                    <li {{ Request::is('/showLastAddAnnouncements') ? 'active' : '' }}><a href="/announcements">Ogłoszenia</a></li>
                @else()
                    <li class="{{(Request::is('/allArticlesConfirmed') ? 'active' : '')}}"><a href="/allArticlesConfirmed">Wiadomości<span class="sr-only"></span></a></li>
                    <li {{ Request::is('/showLastAddAnnouncements') ? 'active' : '' }}><a href="/showLastAddAnnouncements">Ogłoszenia</a></li>
                @endif
                    {{--<li {{ Request::is('/showLastAddAnnouncements') ? 'active' : '' }}><a href="/showLastAddAnnouncements">Ogłoszenia</a></li>--}}
                @if (!(Auth::guest())&&(!(Auth::user()->status=='admin')))
                    <li {{ Request::is('/addAnnounce') ? 'active' : '' }}><a href="/addAnnounce">Dodaj ogłoszenie</a></li>
                @endif




            </ul>


            <ul class="nav navbar-nav navbar-right">
                {{--<li><a href="#">O nas</a></li>--}}
                @if (!(Auth::guest()))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Witaj {{ Auth::user()->name }}<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @php($userId=Auth::user()->id)
                                <li><a href="/myProfile/{{$userId}}"><span class="glyphicon glyphicon-user"></span>   Mój profil</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <span class="glyphicon glyphicon-log-out"></span>
                                        Wyloguj
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                        </ul>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dołącz do nas<span class="caret"></span></a>
                        <ul class="dropdown-menu" style="text-align: center">
                            <li role="menuitem" class="btn-lg"><form><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Zaloguj się</a></form></li>
                            <li role="menuitem" class="btn-lg"><form><a href="/register"> <span class="glyphicon glyphicon-list-alt"></span> Zarejestruj się</a></form></li>
                        </ul>
                    </li>
                @endif

            </ul>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<div >
    @if(Session::has('message'))
        <div class="alert alert-dismissible alert-success">
            <h4 style="margin: 10px 0"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('message') }}</h4>
        </div>

        <hr>
    @endif
    @yield('content')
</div>


<footer class="footer" style="text-align: center; width: 100%!important;">
    <div class="container" >
        <table class="table table-striped" style="width: 100%!important;">
            <thead class="text-center">
            <tr>
                <th class="text-center"><h3>O nas</h3></th>
                <th class="text-center"><h3>Kontakt</h3></th>
                <th class="text-center"><h3>Zgłoś coś</h3></th>
            </tr>
            </thead>
            <tr>
                <td><a href="/ourRedactors">Nasi redaktorzy</a></td>
                <td>superinfoserwis@gmail.com</td>
                <td><a href="/contactMail">Formularz kontaktowy</a></td>
            </tr>
            {{--<tr>
                <td>Lorem ipsum</td>
                <td>Lorem ipsum</td>
                <td>Lorem ipsum</td>
            </tr>--}}
        </table>
    </div>
    <label class="text-justify">
    Copyrights 2017
    Joanna Ryndak
</label>
</footer>
</body>
</html>

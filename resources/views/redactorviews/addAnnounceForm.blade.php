@extends('master')
        <!DOCTYPE html>
<html lang="pl" data-squireinit="true">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/stylesheet.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}"/>

    <script type="text/javascript">
        function MyAlert() {
            {
                var choice = document.getElementById("contactType").value;
                if (choice=== "email") {
                    document.getElementById("phoneNumber").disabled = true;
                } else if (choice=== "phone") {
                    document.getElementById("phoneNumber").disabled = false;
                }
            }
        }
    </script>
</head>
<body>

@section('content')
    <div class="col-xs-8 col-xs-offset-2 " style="background: #f7ffe4; padding: 2%">
<div id ="create_article_form">
    {!! Form::open(['url' => '/submitAnnounce','enctype'=>'multipart/form-data'])!!}
    {!! Form::text('announcementTitle',null,array('class'=>'col-xs-8 col-xs-offset-2','id'=>'announcementTitle','placeholder'=>'Wpisz nazwę twojego ogłoszenia(max.100 znaków)','style'=>'height: 2em','maxlength' => 100,'required' => 'required')) !!}

    {!! Form::hidden('userId',Auth::user()->id) !!}
    {{ Form::select('announcementType',['find'=>'znalezione','lost'=>'zgubione','forSale'=>'na sprzedaż','buy'=>'kupię'],null,array('class'=>'col-xs-8 col-xs-offset-2 ','style'=>'height: 2em','placeholder' => 'Wybierz rodzaj ogłoszenia...','required' => 'required')) }}
    <br>
    {{ Form::select('contactType',['email'=>'e-mail','phone'=>'telefon'],null,array('id'=>'contactType','class'=>'col-xs-8 col-xs-offset-2 ','style'=>'height: 2em','placeholder' => 'Wybierz preferowaną formę kontaktu...','onchange'=>'MyAlert()','required' => 'required')) }}
    <input type="number" name="phone" placeholder="601-102-203" min="1" max="999999999" class="col-xs-8 col-xs-offset-2" id = "phoneNumber" style="height: 2em"  required/>
    <br>
    {!! Form::textarea('announcementContent',null,array('class'=>'col-xs-8 col-xs-offset-2','id'=>'announcementContent','placeholder'=>'Tutaj wpisz treść swojego ogłoszenia(max.255 znaków)','maxlength' => 255,'style'=>'resize:vertical','required' => 'required')) !!}

    <br>
    <button type="reset" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-remove"></span> Wyczyść wszystkie pola</button>
    <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span> Dodaj ogłoszenie</button>
        {{ Form::close() }}

    </div>
</div>

</div>



    </div>
@endsection
</body>
</html>

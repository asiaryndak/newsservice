@extends('master')
        <!DOCTYPE html>
<html lang="pl" data-squireinit="true">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <!-- wysihtml5 parser rules -->
    <!-- Library -->
    <script src="{{asset('js/dist/wysihtml5-0.3.0.min.js')}}"></script>
    <script src="{{asset('js/parser_rules/advanced.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/stylesheet.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
</head>
<body>

@section('content')
    <div class="col-md-8 col-md-offset-2" style="background: #f7ffe4; padding: 2%">
<div id ="create_article_form">
    {!! Form::open(['url' => '/submitArticle','enctype'=>'multipart/form-data'])!!}
    {!! Form::label('Tytuł') !!}
    {!! Form::text('articleTitle',null,array('id'=>'articleTitle','placeholder'=>'Tytuł artykułu','required'=>'required')) !!}
    {!! Form::hidden('userId',null,array('id'=>'userId')) !!}
<div id ="menu_toolbar">
    <div id ="toolbar" style="border: black 1px;color: black;text-align: justify">
<div id="wysihtml5-toolbar" style="display: none;">
    <button type="button" class="btn btn-default" data-wysihtml5-command="bold" title="CTRL+B"><span class="glyphicon glyphicon-bold"></span>Pogrubienie</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="italic" title="CTRL+I"><span class="glyphicon glyphicon-italic"></span>Pochylenie</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="createLink"><span class="glyphicon glyphicon-link"></span>Wstaw link</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="insertImage"><span class="glyphicon glyphicon-file"></span>Wstaw obraz</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1">h1</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2">h2</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="insertUnorderedList"><span class="glyphicon glyphicon-list"></span> Lista nieponumerowana</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="insertOrderedList"><span class="glyphicon glyphicon-list-alt"></span> Lista numerowana </button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="undo">Cofnij</button>
    <button type="button" class="btn btn-default" data-wysihtml5-command="redo">Przywróć</button>
    <button type="button" class="btn btn-default action" data-wysihtml5-action="change_view" title="Show HTML" onclick="showHTML()" unselectable="on">Pokaż kod HTML</button>

    <!-- Some wysihtml5 commands like 'createLink' require extra paramaters specified by the user (eg. href) -->
    <div data-wysihtml5-dialog="createLink" style="display: none;">
        <label>
            Link:
            <input data-wysihtml5-dialog-field="href" value="http://" class="text">
        </label>
        <button class="btn btn-success" data-wysihtml5-dialog-action="save" style="color: white;"><span class="glyphicon glyphicon-ok"></span>OK</button><button class=" btn btn-danger" data-wysihtml5-dialog-action="cancel" style="color: white;"><span class="glyphicon glyphicon-remove"></span>Anuluj</button>
    </div>

    <div data-wysihtml5-dialog="insertImage" style="display: none;">
        <label>
            Obraz
            <input data-wysihtml5-dialog-field="src" value="http://">
        </label>
        <label>
            Położenie:
            <select data-wysihtml5-dialog-field="className">
                <option value="">Domyślnie</option>
                <option value="wysiwyg-float-left">Lewo</option>
                <option value="wysiwyg-float-right">Prawo</option>
            </select>
        </label>
        <button class="btn btn-success" data-wysihtml5-dialog-action="save" style="color: white;"><span class="glyphicon glyphicon-ok"></span>OK&nbsp;</button><button class="btn btn-danger" data-wysihtml5-dialog-action="cancel" style="color: white;"><span class="glyphicon glyphicon-remove"></span>Anuluj</button>
    </div>
</div>
    </div>
    <div class="textField ">

{!!Form::textarea('wysihtml5-textarea',null,array('id'=>'wysihtml5-textarea','placeholder'=>'Wprowadź treść artykułu', 'autofocus','required'=>'required'))!!}
        <br>
        {!! Form::label('Wybierz rodzaj artykułu',null,array('id'=>'type','required'=>'required')) !!}
{{--        {!! Form::select('typeOfArticle',array('news','historia','ciekawostka')) !!}--}}
        {!! Form::select('typeOfArticle',['news'=>'news','history'=>'historia','curiosity'=>'ciekawostka'],array('required'=>'required')) !!}




        {{ csrf_field() }}
        <div class="form-group">
            <label for="imageInput"></label>
            <input data-preview="#preview" name="input_img" type="file" id="imageInput">
            <img class="col-sm-6" id="preview"  src="" ></img>
            <p class="help-block">Wybierz zdjęcie, które chcesz dodać do swojego artykułu.</p>
        </div>


        {!! Form::submit('ZAPISZ',['class'=>'btn btn-success']) !!}
        {{ Form::close() }}

    </div>
</div>

</div>


<script>
    var editor = new wysihtml5.Editor("wysihtml5-textarea", { // id of textarea element
        toolbar:      "wysihtml5-toolbar", // id of toolbar element
        parserRules:  wysihtml5ParserRules // defined in parser rules set
    });
</script>

<script>
    function showHTML(){
        $('#textarea').wysihtml5({
            "html": true
        });
    }
</script>

    </div>
@endsection
</body>
</html>

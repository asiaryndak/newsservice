@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="announceDetails">

                            <h6>Ogłoszenie dodane : {{$announcement->created_at}}</h6>
                            @php($finishDate = $announcement->updated_at->addDays(7))
                                @if($finishDate<$today=(\Carbon\Carbon::now()))
                                    <h6>Ogłoszenie jest już nieaktywne</h6>
                                @else
                                    <h6>Wygasa: {{$finishDate}}</h6>
                                @endif
                            <h1 class="title text-center text-uppercase">{{$announcement->announcementTitle}}</h1>
                                @php($announType='')
                                @php($aType = $announcement->announcementType)
                                    @if($aType=='buy')
                                            @php($announType='kupię')
                                        @elseif($aType=='forSale')
                                            @php($announType='na sprzedaż')
                                        @elseif($aType=='find')
                                            @php($announType='znalezione')
                                        @else
                                        @php($announType='zgubione')
                                            @endif
                                <h4 class="text-center text-uppercase">{{$announType}}</h4>
                            @php($contType='')
                            @php($author=\App\User::find($announcement->announceAuthorId))
                            @if($announcement->contactType=='phone')
                                @php($contType='Telefon')
                            <button id="phoneNumber" class="pull-right" style="pointer-events: none"><span class="glyphicon glyphicon-phone "></span> {{$contType}}: {{$announcement->phone}}</button>
                            @else
                                @php($contType='E-mail')
                            <button id ="email" class="pull-right" style="pointer-events: none"><span class="glyphicon glyphicon-envelope"></span> {{$contType}}: {{$author->email}}</button>
                            @endif


                                <div class="table-bordered col-md-8 col-md-offset-2" style="margin-top: 2em; margin-bottom: 2em">
                                    <article>{{$announcement->announcementContent}}</article>
                                </div>
                        @if(!Auth::guest())
                            @php($authorId=$author->id)
                            @php($category=$announcement->announcementType)
                                   <a href="/showUserAnnouncements/{{$authorId}}"> <button class="pull-right btn-primary btn-sm">Zobacz inne ogłoszenia
                                        użytkownika {{$author->name}} {{$author->lastName}}</button></a>
                                    <a href="/showAnnouncementsFromCategory/{{$category}}"><button class="pull-left btn-primary btn-sm">Zobacz inne ogłoszenia z tej kategorii</button></a>
                       @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



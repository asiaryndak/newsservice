@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                {{--<div class="panel-heading">bestNews.pl</div>--}}

                <div class="panel-body">

                    <div class="table-bordered">
                                <div class="col-xs-6 pull-left">

                                    <div class="table-responsive panel-heading bg-success">
                                        <h3 class="text-center text-info"><span class="glyphicon glyphicon-ok"></span> Twoje najnowsze zatwierdzone artykuły</b> </h3>

                                        <table class="table">
                                            <tr>
                                                <th>Tytuł artykułu</th>
                                                <th>Treść</th>
                                                <th>Data dodania</th>
                                            </tr>
                                            @foreach($yoursAcceptedArticles as $article)
                                                <tr id="{{$article->articleId}}" onclick='routing("/articleDetails/{{$article->articleId}}")' onmouseover = "changeColor({{ $article->articleId }})" onmouseout="mouseOut({{ $article->articleId }})">
                                                    <td> {!! str_limit($article->articleTitle, 50) !!}</td>
                                                    <td>{!! str_limit($article->articleContent, 50) !!}</td>
                                                    <td>{{$article->created_at}}</td>

                                                </tr>
                                            @endforeach
                                        </table>

                                        <div class="text-center">{{$yoursAcceptedArticles->appends('rejectedArticles', \Illuminate\Support\Facades\Input::get('rejectedArticles',1))->appends('announcementsActive', \Illuminate\Support\Facades\Input::get('announcementsActive',1))->appends('announcementsOff', \Illuminate\Support\Facades\Input::get('announcementsOff',1))->links()}}</div>
                                    </div>
                                    <hr>
                                </div>
                    </div>


                    <div class="table-bordered">
                        <div class="col-xs-6 pull-right">

                            <div class="table-responsive panel-heading bg-warning">
                                <h3 class="text-center text-info"><span class="glyphicon glyphicon-remove"></span> Twoje artykuły, które zostały odrzucone</b> </h3>

                                <table class="table">
                                    <tr>
                                        <th>Tytuł artykułu</th>
                                        <th>Treść</th>
                                        <th>Data dodania</th>
                                    </tr>
                                    @foreach($yoursDeclineArticles as $article)
                                        <tr id="{{$article->articleId}}" onclick='routing("/articleDetails/{{$article->articleId}}")' onmouseover = "changeColor({{ $article->articleId }})" onmouseout="mouseOut({{ $article->articleId }})">
                                            <td>{!! str_limit($article->articleTitle, 25) !!}</td>
                                            <td>{!! str_limit($article->articleContent, 50) !!}</td>
                                            <td>{{$article->created_at}}</td>

                                        </tr>
                                    @endforeach
                                </table>

                                <div class="text-center">{{$yoursDeclineArticles->appends('acceptedArticles', \Illuminate\Support\Facades\Input::get('acceptedArticles',1))->appends('announcementsActive', \Illuminate\Support\Facades\Input::get('announcementsActive',1))->appends('announcementsOff', \Illuminate\Support\Facades\Input::get('announcementsOff',1))->links()}}</div>
                            </div>
                            <hr>
                        </div>
                    </div>


                    <div class="col-xs-6 pull-right">
                    <div class = "list-group text-center btn-group">
                        <a href="/addArticle"><button id="addArticle" class="btn btn-success btn-m col-xs-12" >DODAJ NOWY ARTYKUŁ</button></a>
                        <a href="/showMyArticles"><button id="showMyArticles" class="btn btn-info btn-m col-xs-12">ZOBACZ SWOJE ARTYKUŁY</button></a>
                        <a href="/showLastAddedArticles"><button id="showLastAddedArticles" class="btn btn-danger btn-m col-xs-12">ZOBACZ OSTATNIO DODANE ARTYKUŁY</button></a>
                        <a href="/addAnnounce"><button id="addAnnounce" class="btn btn-primary btn-m col-xs-12">DODAJ OGŁOSZENIE</button></a>
                        <a href="/showLastAddAnnouncements"><button id="showNewAnnounce" class="btn btn-warning btn-m col-xs-12">ZOBACZ OSTATNIE OGŁOSZENIA</button></a>
                        {{--<button id="showQuestions" class="btn btn-secondary btn-lg">PYTANIA</button>--}}
                    </div>
                    </div>


                        <div class="table-bordered">
                            <div class="col-xs-6 pull-right">

                                <div class="table-responsive panel-heading bg-info">
                                    <h3 class="text-center text-info"><span class="glyphicon glyphicon-time"></span> Twoje aktywne ogłoszenia</b> </h3>

                                    <table class="table">
                                        <tr>
                                            <th>Tytuł</th>
                                            <th>Treść</th>
                                            <th>Dodane</th>
                                            <th>Wygasa</th>
                                        </tr>
                                        @foreach($yoursAnnouncements as $announce)
                                            <tr id="{{$announce->announcementId}}" onclick='routing("/announcementDetails/{{$announce->announcementId}}")' onmouseover = "changeColor({{ $announce->announcementId }})" onmouseout="mouseOut({{ $announce->announcementId }})">
                                                <td> {{$announce->announcementTitle}}</td>
                                                <td>{!! str_limit($announce->announcementContent, 50) !!}</td>
                                                <td>{{$announce->created_at}}</td>
                                                @php($finishDate = $announce->updated_at->addDays(7))
                                                <td>{{$finishDate}}</td>
                                            </tr>
                                        @endforeach
                                    </table>

                                    <div class="text-center">{{$yoursAnnouncements->appends('acceptedArticles', \Illuminate\Support\Facades\Input::get('acceptedArticles',1))->appends('rejectedArticles', \Illuminate\Support\Facades\Input::get('rejectedArticles',1))->appends('announcementsOff', \Illuminate\Support\Facades\Input::get('announcementsOff',1))->links()}}</div>
                                </div>
                                <hr>
                            </div>

                </div>

                    <div class="col-xs-6 pull-left">

                        <div class="table-responsive panel-heading bg-danger">
                            <h3 class="text-center text-info"><span class="glyphicon glyphicon-off"></span> Twoje przedawnione ogłoszenia</b> </h3>

                            <table class="table">
                                <tr>
                                    <th>Tytuł</th>
                                    <th>Treść</th>
                                    <th>Dodane</th>
                                </tr>
                                @foreach($yoursAnnouncementsOff as $announce)
                                    <tr id="{{$announce->announcementId}}" onclick='routing("/announcementDetails/{{$announce->announcementId}}")' onmouseover = "changeColor({{ $announce->announcementId }})" onmouseout="mouseOut({{ $announce->announcementId }})">
                                        <td> {{$announce->announcementTitle}}</td>
                                        <td>{!! str_limit($announce->announcementContent, 50) !!}</td>
                                        <td>{{$announce->created_at}}</td>
                                    </tr>
                                    <td><a href ="/refreshAnnouncement/{{$announce->announcementId}}"><button class="btn btn-sm btn-success"><span class="glyphicon glyphicon-refresh"></span> Odnów</button> </a></td>

                                @endforeach
                            </table>

                            <div class="text-center">{{$yoursAnnouncementsOff->appends('acceptedArticles', \Illuminate\Support\Facades\Input::get('acceptedArticles',1))->appends('announcementsActive', \Illuminate\Support\Facades\Input::get('announcementsActive',1))->appends('rejectedArticles', \Illuminate\Support\Facades\Input::get('rejectedArticles',1))->links()}}</div>
                        </div>
                        <hr>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function routing(url) {
        window.location.href = url;
    }
</script>

<script>
    function changeColor(name) {
        document.getElementById(name).style.color = "green";
//                document.getElementById(name).style.fontWeight = "bold";
    }

    function mouseOut(name) {
        document.getElementById(name).style.color = "black";
//                document.getElementById(name).style.fontWeight = "normal";

    }
</script>
@endsection

@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">

                    </div>

                    <div class="panel-body">
                        <div class="redactor_articles html-container">
                            @foreach ($announcements as $announcement)

                                <div class="btn-group" role="group" aria-label="Buttons info">
                                    @php($author = \App\User::find($announcement->announceAuthorId))
                                        @if($author->id ==Auth::user()->id)
                                            <button class="btn btn-sm btn-default btn-secondary"
                                                    style="text-align: right;pointer-events: none"><span
                                                        class="glyphicon glyphicon-star"></span> Twoje ogłoszenie
                                            </button>
                                        @else
                                            <button class="btn btn-sm btn-default btn-secondary"
                                                    style="text-align: right;pointer-events: none">Autor
                                                : {!! $author->name !!} {!! $author->lastName !!}</button>
                                        @endif
                                        <button class="btn btn-sm btn-success btn-secondary"
                                                style="pointer-events: none">Data
                                            rozpoczęcia: {!! $announcement->created_at !!}</button>
                                        @php($finishDate = $announcement->updated_at->addDays(7))
                                            <button class="btn btn-sm btn-danger btn-secondary"
                                                    style="pointer-events: none"><span
                                                        class="glyphicon glyphicon-time"></span> Data
                                                wygaśnięcia: {!! $finishDate !!}</button>
                                            @if($finishDate<$today=(\Carbon\Carbon::now()))
                                                <button class="btn btn-sm btn-default" style="pointer-events: none">
                                                    Zakończone
                                                </button>
                                                    @if($author->id ==Auth::user()->id)
                                                        <a href="/refreshAnnouncement/{{$announcement->announcementId}}">
                                                            <button class="btn btn-lg btn-default pull-right "
                                                                    style="text-align: right; margin-top: 2em"><span
                                                                        class="glyphicon glyphicon-refresh"></span> Odnów ogłoszenie
                                                            </button>
                                                        </a>
                                                    @endif
                                            @else
                                                @php($daysUntilFinish = $finishDate->diffInDays(\Carbon\Carbon::now()))
                                                    <button class="btn btn-sm btn-info btn-secondary"
                                                            style="pointer-events: none">
                                                        Pozostało: {!! $daysUntilFinish !!}dni
                                                    </button>

                                             @endif

                                </div>
                                <h3>{!! $announcement->announcementTitle !!}</h3>
                                {!! str_limit($announcement->announcementContent, 100) !!}

                                <a class="btn btn-primary"
                                   href="/announcementDetails/{{$announcement->announcementId}}">Szczegóły</a>
                                <hr>
                            @endforeach
                            {{--Uzyskanie linków do kolejnych stron wyświetlanych artykułów--}}
                            {{$announcements->links()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



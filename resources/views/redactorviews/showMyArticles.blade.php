@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body" >

                    <div class = "redactor_articles html-container">
                        @foreach ($articles as $article)

                            @if($article->articleStatus=='waiting'&&$article->articleStatus!=null)
                            @php($status='oczekujący')
                            @elseif ($article->articleStatus=='confirmed'&&$article->articleStatus!=null)
                            @php($status='zatwierdzony')
                            @elseif ($article->articleStatus=='archive'&&$article->articleStatus!=null)
                            @php($status='zarchiwizowany')
                            @elseif ($article->articleStatus=='rejected'&&$article->articleStatus!=null)
                            @php($status='odrzucony')
                            @else
                            @php($status='nieznany')
                            @endif

                            <h6><button class="btn btn-info btn-sm" style="pointer-events: none">Status : {!! $status !!}</button></h6>
                            <h3>{!! $article->articleTitle !!}</h3>
                            {!! str_limit($article->articleContent, 100) !!}
                        @php($articleId = $article->articleId)
                        @php($img =DB::table('article_attachments')->where('articleId','=',$articleId)->get())

                            @foreach($img as $im)
{{--                                {{$src = "C:\Users\Joanna\PhpstormProjects\projekt_inzynierski\storage/app/".$im->attachmentLink}}--}}

                            {{--<img src="{{$src}}" class="img-thumbnail" style="max-height: 250px;max-width: 250px"/>--}}
                            <img src="{{$im->attachmentLink}}" class="img-thumbnail" style="max-height: 250px;max-width: 250px"/>
                            @endforeach
                            <br><a class="btn btn-primary" href="/articleDetails/{{$article->articleId}}"><span class="glyphicon glyphicon-list-alt"></span>  Szczegóły</a>
                                @if($status=='oczekujący')
                                    <a href="/editArticle/{{$article->articleId}}"><button class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edytuj</button></a>
                                    <a href="/deleteMyArticle/{{$article->articleId}}"><button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Usuń</button></a>
                                @endif
                            <hr>
                        @endforeach
                        {{--Uzyskanie linków do kolejnych stron wyświetlanych artykułów--}}
                        {{$articles->links()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection



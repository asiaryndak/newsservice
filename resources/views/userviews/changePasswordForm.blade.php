@extends('master')
        <!DOCTYPE html>
<html lang="pl" data-squireinit="true">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Edytowanie profilu</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/stylesheet.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}"/>
</head>
<body>

@section('content')
    <div class="col-md-8 col-md-offset-2" style="background: #f7ffe4; padding: 2%">

<div id ="create_article_form">
{{--    @php(--}}
    {!! Form::open(['url' => 'updatePassword'])!!}

{{--    {!! Form::label('uPass','Obecne hasło',array('class'=>'center-block input-lg text-center')) !!}
    {!! Form::password('oldPassword',array('class'=>'center-block input-lg','placeholder'=>'Obecne hasło','required'=>'required')) !!}<hr>
    {!! Form::label('uNewPass','Nowe hasło',array('class'=>'center-block input-lg text-center')) !!}
    {!! Form::password('password',array('class'=>'center-block input-lg','placeholder'=>'Nowe hasło','required'=>'required')) !!}<hr>
    {!! Form::label('uNewPassConfirm','Potwierdź nowe hasło',array('class'=>'center-block input-lg text-center')) !!}
    {!! Form::password('password_confirmation',array('class'=>'center-block input-lg','placeholder'=>'Potwierdź nowe hasło','required'=>'required')) !!}<hr>--}}

    <label for="textArea" class="control-label col-sm-12 text-center">Wpisz obecne hasło</label>
    <div class="form-group{{ $errors->has('oldPassword') ? ' has-error' : '' }}">
        <div class="form-group col-md-6 col-sm-offset-3">
            <input style="text-align: center" id="oldPassword" type="password" class="form-control ce" name="oldPassword"  required >

            @if ($errors->has('oldPassword'))
                <span class="help-block">
                                        <strong>{{ $errors->first('oldPassword') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <label for="textArea" class="control-label col-sm-12 text-center">Wpisz nowe hasło</label>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="form-group col-md-6 col-sm-offset-3">
            <input style="text-align: center" id="password" type="password" class="form-control" name="password"  required >

            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <label for="textArea" class="control-label col-sm-12 text-center">Powtórz nowe hasło</label>
    <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
        <div class="form-group col-md-6 col-sm-offset-3">
            <input style="text-align: center" id="password-confirm" type="password" class="form-control" name="password_confirmation" required >

            @if ($errors->has('password-confirm'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
            @endif
        </div>
    </div>


        {!! Form::submit('Zapisz zmiany w profilu',['class'=>'btn btn-success pull-right btn-lg']) !!}
    <a href="{{URL::previous()}}">{!! Form::button('Powróć bez zapisywania zmian',['class'=>'btn btn-warning btn-lg']) !!}</a>
    {{ Form::close() }}

    </div>
</div>

</div>

    </div>

@endsection
</body>
</html>

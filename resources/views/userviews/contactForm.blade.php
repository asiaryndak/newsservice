@extends('master')

@section('content')
    <div class="jumbotron container-fluid">
        <div class="col-xs-6 col-xs-offset-3 table-bordered bg-pink ">
    {{Form::open(['url' => '/sendContactMail'])}}
            <h3 class="text-center"><label class="center-block"><span class="glyphicon glyphicon-pencil"></span> Formularz kontaktowy:</label></h3>
        @if(Auth::guest())
            {{Form::email('contactEmail',null,array('required'=>'required','class'=>'center-block col-xs-12',
        'placeholder'=>'Tutaj podaj swój adres email na który wyślemy odpowiedź...','style'=>'height:3em'))}}
            @else()
            @php($user = \App\User::find(Auth::user()->id))
                @php($mail = $user->email)
                        {{Form::email('contactEmail',$mail,array('required'=>'required','class'=>'center-block col-xs-12',
                        'style'=>'height:3em', 'readonly'=>'readonly'))}}
            @endif
            {{Form::text('title',null,array('required'=>'required','class'=>'center-block col-xs-12',
            'placeholder'=>'Wpisz tytuł swojego zgłoszenia...','style'=>'height:3em'))}}
        {{Form::textarea('contentMail',null,array('required'=>'required','class'=>'center-block col-xs-12',
        'placeholder'=>'Tutaj wpisz treść...','style'=>'resize:vertical'))}}
            <button type="submit" class="btn btn-lg btn-success col-xs-6 pull-right"><span class="glyphicon glyphicon-envelope"></span> Wyślij maila</button>
    {{Form::close()}}
        </div>
    </div>
@endsection
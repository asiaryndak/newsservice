@extends('master')
        <!DOCTYPE html>
<html lang="pl" data-squireinit="true">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Edytowanie profilu</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/stylesheet.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}"/>
</head>
<body>

@section('content')
    <div class="col-md-8 col-md-offset-2" style="background: #f7ffe4; padding: 2%">
<div id ="create_article_form">
{{--    @php(--}}
    {!! Form::open(['url' => '/updateProfile'])!!}
    {!! Form::label('uname','Imię',array('class'=>'center-block input-lg text-center','maxlength' => 100)) !!}
    {!! Form::text('name',$user->name,array('id'=>'name','required'=>'required', 'class'=>'center-block input-lg')) !!}<hr>
    {!! Form::label('uLname','Nazwisko',array('class'=>'center-block input-lg text-center')) !!}
    {!! Form::text('lastName',$user->lastName,array('id'=>'name','required'=>'required', 'class'=>'center-block input-lg','maxlength' => 100)) !!}<hr>
    {!! Form::hidden('userId',$user->id,array('id'=>'userId')) !!}
    {!! Form::label('emailLabel','Adres e-mail',array('class'=>'center-block input-lg text-center')) !!}
    {!! Form::email('email',$user->email,array('id'=>'email','required'=>'required', 'class'=>'center-block input-lg','maxlength' => 100)) !!}<hr>
        {!! Form::submit('Zapisz zmiany w profilu',['class'=>'btn btn-success pull-right btn-lg']) !!}
    <a href="/myProfile/{{Auth::user()->id}}">{!! Form::button('Powróć bez zapisywania zmian',['class'=>'btn btn-warning btn-lg']) !!}</a>
        {{ Form::close() }}

    </div>
</div>

</div>

    </div>

@endsection
</body>
</html>

@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>

                <div class="panel-body" >

                    <div class = "redactor_articles html-container">
                        @foreach ($lastArticles as $lastArticle)

                            @if($lastArticle->articleStatus=='waiting'&&$lastArticle->articleStatus!=null)
                            @php($status='oczekujący')
                            @elseif ($lastArticle->articleStatus=='confirmed'&&$lastArticle->articleStatus!=null)
                            @php($status='zatwierdzony')
                            @elseif ($lastArticle->articleStatus=='archive'&&$lastArticle->articleStatus!=null)
                            @php($status='zarchiwizowany')
                            @elseif($lastArticle->articleStatus=='rejected'&&$lastArticle->articleStatus!=null)
                            @php($status='odrzucony')
                            @else
                            @php($status='nieznany')
                            @endif

                            @php($author = \App\User::find($lastArticle->articleAuthorId))
                            <button class="btn btn-info btn-sm" style="pointer-events: none">Status : {!! $status !!}</button>
                            @if($author->id==Auth::user()->id)
                            <button class="btn btn-sm btn-danger" style="text-align: right;pointer-events: none"><span class="glyphicon glyphicon-star"></span> Twój artykuł</button>
                            @else
                            <button class="btn btn-sm btn-primary" style="text-align: right;pointer-events: none">Autor : {!! $author->name !!} {!! $author->lastName !!}</button>
                            @endif
                            <button class="btn btn-sm btn-default" style="pointer-events: none">Data dodania: {!! $lastArticle->created_at !!}</button>

                            <h3>{!! $lastArticle->articleTitle !!}</h3>
                            {!! str_limit($lastArticle->articleContent, 100) !!}
                        @php($articleId = $lastArticle->articleId)
                        @php($img =DB::table('article_attachments')->where('articleId','=',$articleId)->get())

                            @foreach($img as $im)
                            <img src="{{$im->attachmentLink}}" class="img-thumbnail" style="max-height: 250px;max-width: 250px"/>
                            @endforeach
                                                                            <div class="buttons">
                            <a class="btn btn-primary" href="/articleDetails/{{$lastArticle->articleId}}"><span class="glyphicon glyphicon-list-alt"></span> Szczegóły</a>
                            @if(Auth::user()->status=='admin')
                                <a class="btn btn-success" href="/editArticle/{{$lastArticle->articleId}}"><span class="glyphicon glyphicon-edit"></span> Edytuj</a>
                                <a href="/deleteArticle/{{$lastArticle->articleId}}"><button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Usuń</button> </a>

                                    @if($lastArticle->articleStatus=='confirmed')
                                        <a href="/rejectArticle/{{$lastArticle->articleId}}"><button class="btn btn-warning"><span class="glyphicon glyphicon-ban-circle"></span> Odrzuć</button> </a>
                                        <a href="/archiveArticle/{{$lastArticle->articleId}}"><button class="btn btn-default"><span class="glyphicon glyphicon-copy"></span> Archwizuj</button> </a>
                                    @elseif($lastArticle->articleStatus=='waiting')
                                        <a href="/confirmArticle/{{$lastArticle->articleId}}"><button class="btn btn-success"><span class="glyphicon glyphicon-check"></span> Zatwierdź</button> </a>
                                        <a href="/rejectArticle/{{$lastArticle->articleId}}"><button class="btn btn-warning"><span class="glyphicon glyphicon-ban-circle"></span> Odrzuć</button> </a>
                                        <a href="/archiveArticle/{{$lastArticle->articleId}}"><button class="btn btn-default"><span class="glyphicon glyphicon-copy"></span> Archwizuj</button> </a>
                                    @elseif($lastArticle->articleStatus=='rejected')
                                        <a href="/archiveArticle/{{$lastArticle->articleId}}"><button class="btn btn-default"><span class="glyphicon glyphicon-copy"></span> Archwizuj</button> </a>
                                    @elseif($lastArticle->articleStatus=='archive')
                                        <a href="/unarchiveArticle/{{$lastArticle->articleId}}"><button class="btn btn-warning"><span class="glyphicon glyphicon-paste"></span> Przywróć</button> </a>
                                    @endif

                            @endif
                            <hr>
                                                                            </div>
                        @endforeach
                        {{--Uzyskanie linków do kolejnych stron wyświetlanych artykułów--}}
                        {{$lastArticles->links()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="redactor_articles html-container">
                            @if($article->articleStatus=='waiting'&&$article->articleStatus!=null)
                                @php($status='oczekujący')
                                    <button class="btn btn-info btn-sm" style="pointer-events: none"><span
                                                class="glyphicon glyphicon-time"></span> Status : {!! $status !!}
                                    </button>
                                    @if(($article->articleAuthorId==Auth::user()->id)|| (Auth::user()->status=='admin'))
                                        <a href="/editArticle/{{$article->articleId}}">
                                            <button class="btn btn-primary btn-sm"><span
                                                        class="glyphicon glyphicon-edit"></span> Edytuj
                                            </button>
                                        </a>
                                        @if(Auth::user()->status=='admin')
                                            <a href="/confirmArticle/{{$article->articleId}}">
                                                <button class="btn btn-sm btn-success">Zatwierdź</button>
                                            </a>
                                            <a href="/rejectArticle/{{$article->articleId}}">
                                                <button class="btn btn-sm btn-warning">Odrzuć</button>
                                            </a>

                                        @endif
                                    @endif
                                    @elseif ($article->articleStatus=='confirmed'&&$article->articleStatus!=null)
                                        @php($status='zatwierdzony')
                                            <button class="btn btn-success btn-sm" style="pointer-events: none"><span
                                                        class="glyphicon glyphicon-ok"></span> Status : {!! $status !!}
                                            </button>
                                            @endif

                                            <h1 class="title text-center text-uppercase">{!! $article ->articleTitle!!}</h1>
                                            <button class="btn btn-sm btn-default pull-right"
                                                    style="pointer-events: none">Data ostatniej
                                                edycji: {!! $article->updated_at !!}</button>
                                            <button class="btn btn-sm btn-default pull-right"
                                                    style="pointer-events: none">Data
                                                stworzenia: {!! $article->created_at !!}</button>
                                            <article style="padding: 5em 1em"
                                                     class="text-justify">{!! $article ->articleContent!!}</article>
                                            @foreach($articleAttachments as $articleAttachment)
                                                @php($src = $articleAttachment->attachmentLink )
                                                    <img src="{{$src}}" class="img-thumbnail center-block"/>
                                                    @endforeach


                                                    <hr>
                                                    @if(!(Auth::user()))
                                                        <h3 class="text-justify"><i>Zaloguj się, aby zobaczyć komentarze, a także móc
                                                                komentować.</i></h3>
                                                    @elseif(Auth::user())
                                                        <div class="commentsDiv">
                                                            <h3 class="text-center text-uppercase"> Komentarze
                                                                użytkowników</h3>
                                                            <div class="col-xs-8 col-xs-offset-2">
                                                                {!! Form::open(['url' => '/addComment','enctype'=>'multipart/form-data'])!!}
                                                                {!! Form::textarea('commentText',null,array('required'=>'required','style'=>'resize:none')) !!}
                                                                {!! Form::hidden('commentAuthorId',Auth::user()->id) !!}
                                                                {!! Form::hidden('articleId',$article->articleId) !!}
                                                                {!! Form::submit('Dodaj komentarz',array('class'=>'btn btn-success center-block')) !!}
                                                                {!! Form::close() !!}
                                                            </div>
                                                            @foreach($comments as $comment)
                                                                <div class="label-info label-sm col-md-8 col-md-offset-2 table-bordered">
                                                                    <h6>Dodany : {{$comment->created_at}}</h6>
                                                                    @php($author=\App\User::find($comment->commentAuthorId))
                                                                        @if($author->id==Auth::user()->id||Auth::user()->status=='admin')
                                                                            <a href="/deleteMyComment/{{$comment->commentId}}">
                                                                                <button class="btn-default pull-right btn-sm">
                                                                                    <span class="glyphicon glyphicon-remove"></span>
                                                                                    Usuń komentarz
                                                                                </button>
                                                                            </a>
                                                                        @endif
                                                                        <h5>
                                                                            Autor: {{$author->name}} {{$author->lastName}}</h5>
                                                                        <h4 class="col-xs-10 col-xs-offset-1 "
                                                                            style="background: lightgray; border-radius: 5px; color: black">{{$comment->commentText}}</h4>


                                                                        <br>
                                                                </div>
                                                                <br>

                                                            @endforeach
                                                        </div>
                                                    @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@extends('master')
@section('content')
    <div class="jumbotron container-fluid">
        <div class="table-bordered col-xs-8 col-xs-offset-2 text-center" style="background: rgba(160, 160, 160, 1.0)">
            <h2 class="text-center text-uppercase" style="color: white; text-shadow: rgba(22,22,22,0.56)">Nasi redaktorzy</h2>
            <hr>
            <div class="col-xs-10 col-xs-offset-1" style="background: whitesmoke">
            @foreach($redactors as $redactor)
                @if(Auth::guest()||!(Auth::user()->id===$redactor->id))
                @php($confirmArticlesCount = \App\Article::where('articleAuthorId',$redactor->id)->count('*'))
                    @php($today = \Carbon\Carbon::today())
                        @php($weekAgo = $today->subDays(7))
                @php($activeAnnouncements = \App\Announcement::where('announceAuthorId',$redactor->id)->whereDate('updated_at','>',$weekAgo)->count('*'))
                <h3>{{$redactor->name}} {{$redactor->lastName}}</h3>
                <h4>W naszym serwisie od: {{$redactor->created_at}}</h4>
                <h5>Opublikowanych artykułów: {{$confirmArticlesCount}}</h5>
                    <h6>Aktywnych ogłoszeń: {{$activeAnnouncements}}</h6>
                <hr><hr>
                @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
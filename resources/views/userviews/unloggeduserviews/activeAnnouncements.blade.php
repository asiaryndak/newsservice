@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">

                    </div>

                    <div class="panel-body">
                        <div class="redactor_articles html-container">
                            @foreach ($lastAnnouncements as $lastAnnouncement)
                            @php($finishDate = $lastAnnouncement->updated_at->addDays(7))
                                    @if($finishDate>=$today=(\Carbon\Carbon::now()))

                                <div class="btn-group" role="group" aria-label="Buttons info">
                                    @php($author = \App\User::find($lastAnnouncement->announceAuthorId))
                                        <button class="btn btn-sm btn-default btn-secondary"
                                                    style="text-align: right;pointer-events: none">Autor
                                                : {!! $author->name !!} {!! $author->lastName !!}</button>

                                        <button class="btn btn-sm btn-success btn-secondary"
                                                style="pointer-events: none">Data
                                            rozpoczęcia: {!! $lastAnnouncement->created_at !!}</button>
                                            <button class="btn btn-sm btn-danger btn-secondary"
                                                    style="pointer-events: none"><span
                                                        class="glyphicon glyphicon-time"></span> Data
                                                wygaśnięcia: {!! $finishDate !!}</button>

                                                @php($daysUntilFinish = $finishDate->diffInDays(\Carbon\Carbon::now()))
                                                    <button class="btn btn-sm btn-info btn-secondary"
                                                            style="pointer-events: none">
                                                        Pozostało: {!! $daysUntilFinish !!}dni
                                                    </button>



                                </div>
                                <h3>{!! $lastAnnouncement->announcementTitle !!}</h3>
                                {!! str_limit($lastAnnouncement->announcementContent, 100) !!}

                                <a class="btn btn-primary"
                                   href="/announcementDetails/{{$lastAnnouncement->announcementId}}">Szczegóły</a>
                                <hr>
                                    @endif
                            @endforeach
                            {{--Uzyskanie linków do kolejnych stron wyświetlanych artykułów--}}
                            {{$lastAnnouncements->links()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



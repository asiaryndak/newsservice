<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @section('head')
@endsection


    </head>
    <body>

    @extends('master')

    @section('content')
        {{--<div class="jumbotron col-xs-10">--}}
            <div class="col-xs-10 bg-success jumbotron">
                <h2 class="text-center title text-info"><i>Najnowsze artykuły w serwisie</i></h2>
                @foreach($articlesConfirmed as $article)
                    <div class="table-bordered bg-pink">
                        <h6 class="pull-right"><i>Data dodania: {{$article->updated_at}}</i></h6>
                    <h3><b>{{$article->articleTitle}}</b></h3>
                    <h4><i>{{str_limit($article->articleContent)}}</i></h4>
                        <a href="/userArticleDetails/{{$article->articleId}}"><button class="btn btn-primary">Czytaj więcej...</button> </a>
                    </div>
                    <hr>
                    @endforeach
                <div class="right pull-right"><a href="/allArticlesConfirmed"><button class=" btn btn-primary">Zobacz więcej artykułów</button> </a></div>
{{--                <div class="text-center">{{$articlesConfirmed->links()}}</div>--}}
            </div>
        <div class="col-xs-2 pull-right" style="background: dimgrey; color:white;">
            <h4 class="text-center"><i>Aktywne ogłoszenia</i></h4>
            @foreach($lastActiveAnnouncements as $announce)
                <div class="table-bordered">
                    <h4><b>{{$announce->announcementTitle}}</b></h4>
                    <h6><i>{{str_limit($announce->announcementContent)}}</i></h6>
                    <a href="/announcementDetails/{{$announce->announcementId}}"><button class="btn btn-sm btn-success pull-right">Szczegóły</button> </a>
                </div>
                <br><br>
            @endforeach
            @if(Auth::guest())
            <div class="text-right"><a href="/announcements"><button class="btn btn-default">Zobacz wszystkie ogłoszenia</button> </a> </div>
            @else
            <div class="text-right"><a href="/showLastAddAnnouncements"><button class="btn btn-default">Zobacz wszystkie ogłoszenia</button> </a> </div>
            @endif
        </div>


        <div class="col-xs-2 pull-right table-bordered bg-info" style="margin-top: 3em">
            <h4 class="text-center"><b>Ciekawostki</b></h4>
            @foreach($articlesCuriosityConfirmed as $curiosity)
                <div class="bg-warning">
                    <h6 class="pull-right"><i>Data dodania: {{$curiosity->updated_at}}</i></h6>
                    <h4><b>{{$curiosity->articleTitle}}</b></h4>
                    <h6><i>{{str_limit($curiosity->articleTitle)}}</i></h6>
                    <a href="/userArticleDetails/{{$curiosity->articleId}}"><button class="btn btn-sm btn-success pull-right">Czytaj więcej...</button> </a>
                </div>
                <br><br>
            @endforeach
            <div class="text-center">{{$articlesCuriosityConfirmed->links()}}</div>
        </div>
        {{--</div>--}}
    @stop

    </body>
</html>
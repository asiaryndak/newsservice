<?php
/**
 * Created by PhpStorm.
 * User: Joanna
 * Date: 14.11.2017
 * Time: 14:10
 */
?>

<html>
<head>

</head>
<body>
@extends('master');

@if (!(Auth::guest()))
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body" style="text-align: right">
                        {{--@if (!(Auth::guest()))--}}
                        Witaj {{ Auth::user()->name }} !

                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Wyloguj
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        {{-- @else
                     <form><a href="login">Zaloguj się</a></form>--}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@else
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body" style="text-align: right">
                        <form><a href="login">Zaloguj się</a></form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endif

<div>
    @section('content')
        <div class="col-md-8 col-md-offset-2 news-container" ></div>
        <div class="col-sm-2 col-sm-offset-4 news-container" style="float: right"></div>
        @stop
</div>
</body>
</html>

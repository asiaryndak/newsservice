<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MainPageController@home');

Route::get('/master', function () {
    return view('master');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/wiadomosci', function () {
    return view('userviews.wiadomosci');
});


/**
 * Routing dla obslugi artykulow z poziomu redaktora
 */
Route::get('/addArticle', 'RedactorController@addArticleFormView')->middleware('CheckRedactorLogin');

Route::post('/submitArticle',
    'RedactorController@addArticle')
    ->middleware('CheckRedactorLogin');

Route::post('/updateArticle', 'RedactorController@updateDataInArticle')->middleware('CheckRedactorLogin');
Route::get('/getArticles', 'RedactorController@getArticle')->middleware();
Route::get('/showMyArticles', 'RedactorController@showMyArticles')->name('showMyArticles');
Route::get('articleDetails/{articleId}', 'RedactorController@articleDetails')->middleware('CheckRedactorLogin');
Route::get('/showLastAddedArticles', 'RedactorController@showLastAddedArticles')->middleware('CheckRedactorLogin');
Route::get('editArticle/{articleId}', 'RedactorController@editArticle')->middleware('CheckRedactorLogin');
Route::get('deleteMyArticle/{articleId}', 'RedactorController@deleteArticle')->middleware('CheckRedactorLogin');

/**
 * Metody obsługujące dodawanie,usuwanie,wyświetlanie i edytowanie ogłoszeń z poziomu redaktora
 */
Route::get('/addAnnounce', 'RedactorController@addAnnounceForm')->middleware('CheckPermissions');
Route::post('/submitAnnounce', 'RedactorController@addAnnounce')->middleware('CheckPermissions');
Route::get('/showLastAddAnnouncements', 'RedactorController@showLastAddedAnnouncements')->middleware('CheckPermissions');
Route::get('/announcementDetails/{announcementId}', 'RedactorController@announceDetails');
Route::get('/refreshAnnouncement/{announcementId}', 'RedactorController@refreshAnnouncement')->middleware('CheckRedactorLogin');
Route::get('/showAnnouncementsFromCategory/{announcementType}', 'RedactorController@showAnnouncementsFromCategory')->middleware('CheckPermissions')->name('showAnnouncementsFromCategory');
Route::get('/showUserAnnouncements/{announceAuthorId}', 'RedactorController@showUserAnnouncements')->middleware('CheckPermissions')->name('showUserAnnouncements');


/**
 * Obsługa komentarzy
 */
Route::post('/addComment', 'RedactorController@addCommentToArticle')->middleware('CheckPermissions')->name('addComment');
Route::get("/deleteMyComment/{commentId}", 'RedactorController@deleteMyCommentToArticle')->middleware('CheckPermissions')->name('deleteMyComment');


/**
 * Profil użytkownika
 */
Route::get('myProfile/{userId}', 'UserController@showMyProfile')->middleware('CheckPermissions')->name('myProfile');
Route::get('editProfile/{userId}', 'UserController@editMyProfileView')->middleware('CheckPermissions')->name('editProfileView');
Route::post('updateProfile', 'UserController@updateProfile')->middleware('CheckPermissions')->name('editProfile');
Route::get('changePassword/{userId}', 'UserController@changePasswordView')->middleware('CheckPermissions')->name('changePasswordView');
Route::post('updatePassword', 'UserController@changePassword')->middleware('CheckPermissions')->name('updatePassword');


/**
 * Routing administratora dla zarządzania użytkownikami i ich kontami
 */
Route::get('/showLastRegisteredUsers', 'AdminController@showLastRegisteredUsers')->middleware('CheckAdminLogin')->name('showLastRegisteredUsers');
Route::get('/makeUserAccountActive/{userId}', 'AdminController@makeUserAccountActive')->middleware('CheckAdminLogin');
Route::get('/makeUserAccountDeactivate/{userId}', 'AdminController@makeUserAccountDeactivate')->middleware('CheckAdminLogin');
Route::get('/showUserActivity/{userId}', 'AdminController@showUserActivity')->middleware('CheckAdminLogin');
Route::get('/editUser/{userId}', 'AdminController@editUserView')->middleware('CheckAdminLogin');
Route::post('/editUserSave', 'AdminController@editUser')->middleware('CheckAdminLogin');
Route::get('/deleteUser/{userId}', 'AdminController@deleteUserView')->middleware('CheckAdminLogin');
Route::get('/deleteUserConfirm/{userId}', 'AdminController@deleteUserConfirm')->middleware('CheckAdminLogin');
Route::get('/deleteUserCancel', 'AdminController@deleteUserCancel')->middleware('CheckAdminLogin');
Route::get('/serviceUsers', 'AdminController@serviceUsers')->middleware('CheckAdminLogin')->name('serviceUsers');
Route::get('/showActiveUsersAccounts', 'AdminController@showActiveUsersAccounts')->middleware('CheckAdminLogin')->name('showActiveUsersAccounts');
Route::get('/showUnactiveUsersAccounts', 'AdminController@showUnactiveUsersAccounts')->middleware('CheckAdminLogin')->name('showUnactiveUsersAccounts');
Route::get('/showAllUsers', 'AdminController@showAllUsers')->middleware('CheckAdminLogin');


Route::get('/showOnlyActive', function () {
    return view('adminviews.usersManaging.serviceUsers');
})->middleware('CheckAdminLogin');

Route::get('/showUserDetails/{userId}', 'AdminController@showUserDetails')->middleware('CheckAdminLogin');

/**
 * Admin - zarzadzanie artykulami
 */
Route::get('/rejectArticle/{articleId}', 'AdminController@rejectArticle')->middleware('CheckAdminLogin');
Route::get('/archiveArticle/{articleId}', 'AdminController@archiveArticle')->middleware('CheckAdminLogin');
Route::get('/unarchiveArticle/{articleId}', 'AdminController@unarchiveArticle')->middleware('CheckAdminLogin');
Route::get('/confirmArticle/{articleId}', 'AdminController@confirmArticle')->middleware('CheckAdminLogin');
Route::get('/deleteArticle/{articleId}', 'AdminController@deleteArticleView')->middleware('CheckAdminLogin');
Route::get('/deleteArticleConfirm/{articleId}', 'AdminController@deleteArticle')->middleware('CheckAdminLogin');
Route::get('/deleteArticleCancel', 'AdminController@deleteArticleCancel')->middleware('CheckAdminLogin');
Route::get('/manageArticles', 'AdminController@manageArticlesView')->middleware('CheckAdminLogin');


/**
 * Admin - zarzadzanie ogloszeniami
 */

Route::get('/deleteAnnouncement/{announcementId}', 'AdminController@deleteAnnouncement')->middleware('CheckPermissions');


/**
 * Routing dla uzytkownika
 */
Route::get('/userArticleDetails/{articleId}', 'UserController@userArticleDetails');
Route::get('/allArticlesConfirmed', 'UserController@allArticlesConfirmedView');

Route::get('/contactMail', function () {
        return view('userviews.contactForm');
    });
Route::post('/sendContactMail','UserController@sendContactMail');
Route::get('/ourRedactors','UserController@ourRedactorsView');

Route::get('/announcements','UserController@announcements');


/*Route::get('/404', function () {
    return abort(404,Session::start());
});*/